#pragma once

#include <m_maddie.h>

using namespace std;



class Input {
public:
	using key = uint32_t;
private:
	WNDPROC m_origWndProc{ };
	HWND m_wnd{ };

	bool m_locked{ };
	key m_mouse1{ },
		m_mouse2{ },
		m_mouse4{ },
		m_mouse5{ },
		m_mmouse{ };

	POINTS m_cursor{ };
	array<key, 256> m_keys{ };

	wstring m_iobuf{ };
public:

	inline Input( HWND wnd ) : m_wnd( wnd ) {
	}

	inline WNDPROC attach( WNDPROC hook ) {
		if ( !m_origWndProc )
			m_origWndProc = ( WNDPROC ) SetWindowLongPtr( m_wnd, GWLP_WNDPROC, LONG_PTR( hook ) ); 

		return m_origWndProc;
	}

	inline void detach( ) {
		SetWindowLongPtr( m_wnd, GWLP_WNDPROC, LONG_PTR( m_origWndProc ) );
	}

	inline void run( uint32_t msg, WPARAM wParam, LPARAM lParam ) {
		switch ( msg ) {
		case WM_LBUTTONDOWN:
			m_mouse1 = 1;
			break;
		case WM_LBUTTONUP:
			m_mouse1 = 0;
			break;
		case WM_RBUTTONDOWN:
			m_mouse2 = 1;
			break;
		case WM_RBUTTONUP:
			m_mouse2 = 0;
			break;
		case WM_MBUTTONDOWN:
			m_mmouse = 1;
			break;
		case WM_MBUTTONUP:
			m_mmouse = 0;
			break;
		case WM_XBUTTONDOWN:
			m_mouse5 = 1;
			break;
		case WM_XBUTTONUP:
			m_mouse5 = 0;
			break;
		case WM_MOUSEMOVE:
			m_cursor = MAKEPOINTS( lParam );
			break;
		case WM_KEYDOWN:
			if ( wParam < 256 )
				m_keys[ wParam ] = 1;
			break;
		case WM_KEYUP:
			if ( wParam < 256 )
				m_keys[ wParam ] = 0;
			break;
		case WM_CHAR:
			if ( wParam > 0 && wParam < 0x10000 )
				m_iobuf.push_back( static_cast< wchar_t >( wParam ) );
			break;
		}

	}
	

	inline auto orig( ) { return m_origWndProc;  }
	inline auto wnd( ) { return m_wnd; }
	
	inline bool locked( ) { return m_locked; }

	inline auto& keys( ) { return m_keys; }
	inline auto& mouse1( ) { return m_mouse1; }
	inline auto& mouse2( ) { return m_mouse2; }
	inline auto& mouse4( ) { return m_mouse4; }
	inline auto& mouse5( ) { return m_mouse5; }

	inline void lockInput( ) { m_locked = true; }
	inline void unlockInput( ) { m_locked = false; }

	static Input& inst( HWND wnd = 0 ) {
		static Input* singleton{ };
		if ( singleton == nullptr )
			singleton = new Input( wnd );

		return *singleton;
	}

	static bool pressed( int vk ) {
		static int ran{ };

		ran++;
		if ( !( ran % 1000 ) ) {
			return inst( ).keys( ).at( vk ) != 0;
		}
		return false;
	}

	static key get( int vk ) {
		return inst( ).keys( ).at( vk );
	}
};