#pragma once

#include <m_irenderer.h>
#include <m_xor.h>
#include "m_input.h"

using namespace std;

namespace Gui_lite {
	enum class type : int {
		checkbox,
		hotkey,
		hr,
		slider,
		aliased
	};

	enum class tabs : int {
		aim,
		vis,
		misc
	};

	namespace uicol {
		const Color grey = Color( 50, 50, 50, 255 );
		const Color darkgrey = Color( 20, 20, 20, 255 );
		const Color white = Color( 220, 220, 220, 255 );
		const Color black = Color( 0, 0, 0, 255 );
		const Color hintofgreen = Color( 0, 128, 0, 150 );
		const Color lightgrey = Color( 90, 90, 90, 170 );
		const Color lightergrey = Color( 150, 150, 150, 170 );
		const Color lime = Color( 0, 255, 64, 255 );
		const Color bg = Color( 36, 36, 36, 255 );
	}

	class item {
	public:
		using alias = string;
		using aliases = vector<alias>;

		using callback = void( *)( );

	private:
		string m_name{ },
			m_desc{ };

		float m_val{ },
			m_max{ },
			m_min{ };

		type m_type{ };

		aliases m_aliases{ };
		callback m_callback{ };
	public:

		inline item( const string& name, float* val = nullptr, const string& desc = "", type t = type::checkbox, float min = 0.f, float max = 0.f, callback cb = nullptr, aliases a = { "" } ) :
			m_name( name ), m_val( val ? *val : 0.f ), m_desc( desc ), m_type( t ), m_min( min ), m_max( max ), m_aliases( a ), m_callback( cb )
		{ }
	};

	class group {
	private:
		string m_name;
		vector<item> m_items{ };
	public:

		inline group( const std::string& name ) : m_name( name )
		{ }

		inline auto& name( ) { return m_name; }
		inline auto& items( ) { return m_items; }

		inline void operator += ( item it ) { m_items.push_back( it ); }
	};

	class tab {
	private:
		string m_name;
		vector<group> m_groups{ };
	public:

		inline tab( const std::string& name ) : m_name( name )
		{ }

		inline auto& name( ) { return m_name; }
		inline auto& groups( ) { return m_groups; }

		inline void operator += ( group g ) { m_groups.push_back( g ); }
	};

	class Gui {
	private:
		POINT m_mouse{ };
		IRender* m_render{ };

		vector<tab> m_tabs{ };

		int32_t m_tabsMade{ }, m_groupsMade{ };

		bool m_opened{ };
	public:
		inline void addTab( const string& name ) {
			m_tabs.push_back( tab( name ) );
			m_tabsMade++;
			m_groupsMade = 0;
		}

		inline void addGroup( const string& name ) {
			m_tabs.at( m_tabsMade - 1 ).groups( ).push_back( group( name ) );
			m_groupsMade++;
		}

		inline group* curGroup( ) {
			return &m_tabs.at( m_tabsMade - 1 ).groups( ).at( m_groupsMade - 1 );
		}

		inline void addCheckbox( const string& name, float* val ) {
			auto* cur = curGroup( );
			if ( cur == nullptr )
				return;

			return cur->items( ).push_back( item( name, val ) );
		}

		inline void addSlider( const std::string& name, float* val, float minVal = 0.f, float maxVal = 1.f ) {
			auto* cur = curGroup( );
			if ( cur == nullptr )
				return;

			stringstream desc{ };
			desc << name << STR( " - ( " ) << minVal << STR( " -> " ) << maxVal << STR( " )" );

			return cur->items( ).push_back( item( name, val, desc.str( ), type::slider, minVal, maxVal ) );
		}

		inline void addAliased( const std::string& name, float* aliasVal, item::aliases a ) {
			auto* cur = curGroup( );
			if ( cur == nullptr )
				return;

			auto appendAliases = [ ] ( stringstream& ss, const item::aliases& aliases ) {
				for ( auto& ali : aliases )
					ss << ali << STR( ", " );

				ss << STR( "." );
			};

			stringstream desc{ };
			desc << name << STR( "options: " );
			appendAliases( desc, a );

			float max = static_cast< float >( a.size( ) );

			return cur->items( ).push_back( item( name, aliasVal, desc.str( ), type::aliased, 0.f, max, nullptr, a ) );
		}

		Gui( IRender* renderer ) : m_render( renderer ) 
		{ }	

		

		inline void run( ) {

			if ( Input::get( VK_INSERT ) )
				m_opened = !m_opened;

		}

		Gui( ) { }
		~Gui( ) { }
	};
}