#pragma once

#include "../entity/se_entity.h"

class Prediction {
private:
	Interface GameMovement{ },
		VClientPrediction{ };

	Address IMoveHelper{ },
		m_curCmd{ };

public:
	inline Prediction( ) :
		GameMovement{ Interfaces::inst( )[ STR( "GameMovement" ) ] },
		VClientPrediction{ Interfaces::inst( )[ STR( "VClientPrediction" ) ] }
	{
#if _CSGO
		IMoveHelper = Pattern( STR( "client.dll" ), STR( "8B 0D ?? ?? ?? ?? 8B 46 08 68" ) ).at( 0x2 ).get( 2 );
		m_curCmd = Pattern( STR( "client.dll" ), STR( "89 9F ?? ?? ?? ?? E8 ?? ?? ?? ?? 85 DB" ) ).at( 0x2 );

#if _SE_DEBUG
		io::w_printf( STR( "[Prediction] IMoveHelper ... 0x%X\n" ), IMoveHelper );
		io::w_printf( STR( "[Prediction] m_pCurrrentCommand ... 0x%X\n" ), m_curCmd );
#endif

#endif
	}

#if _CSGO
	inline bool RunCommand( const Entity* ent, const Weapon* wep, const Cmd* cmd ) {
		auto& globals = Client::inst( ).globals( );
		auto& move = std::get<Address>( GameMovement );
		auto& pred = std::get<Address>( VClientPrediction );
		
		if ( !ent || !wep || !cmd )
			return false;

		if ( !IMoveHelper || !m_curCmd )
			return false;
		
		
		float backup = globals.m_flFrameTime;
		globals.m_flFrameTime = globals.m_flIntervalPerTick;

		void* data = move.at( 0x8 ).get<void*>( 1 );
		if ( !data )
			return false;

		bool *inPred = pred.at( 0x8 ).get<bool*>( 1 );
		bool inPredBackup = *inPred;
		*inPred = true;

		move.at( 0x4 ).set<const Entity*>( ent );
		Address( ( uintptr_t ) ent + m_curCmd ).set<const Cmd*>( cmd );

		SetHost( ent );
		SetupMove( ent, cmd, IMoveHelper, data );
		ProcessMovement( ent, data );
		FinishMove( ent, cmd, data );
		SetHost( nullptr );

		*inPred = inPredBackup;
		globals.m_flFrameTime = backup;

		return true;
	}


	// GameMovement
	inline void ProcessMovement( const Entity* ent, void* data ) {
		static Vmt vmt( std::get<Address>( GameMovement ) );
		return vmt.get<void( _tc* )( void*, const Entity*, void* )>( vmt_indexes::GameMovement::ProcessMovement )( std::get<Address>( GameMovement ), ent, data );
	}

	// VClientPrediction
	inline void SetupMove( const Entity* ent, const Cmd* cmd, Address movehelper, void* data ) {
		static Vmt vmt( std::get<Address>( VClientPrediction ) );
		return vmt.get<void( _tc* )( void*, const Entity*, const Cmd*, Address, void* )>( vmt_indexes::VClientPrediction::SetupMove )( std::get<Address>( VClientPrediction ), ent, cmd, movehelper, data );
	}
	inline void FinishMove( const Entity* ent, const Cmd* cmd, void* data ) {
		static Vmt vmt( std::get<Address>( VClientPrediction ) );
		return vmt.get<void( _tc* )( void*, const Entity*, const Cmd*, void* )>( vmt_indexes::VClientPrediction::FinishMove )( std::get<Address>( VClientPrediction ), ent, cmd, data );
	}

	// IMoveHelper
	inline void SetHost( const Entity* ent ) {
		static Vmt vmt( IMoveHelper );
		return vmt.get<void( _tc* )( void*, const Entity* )>( vmt_indexes::IMoveHelper::SetHost )( IMoveHelper, ent );
	}
#endif

	inline auto& pred( ) {
		return std::get<Address>( VClientPrediction );
	}

	static Prediction& inst( ) {
		static Prediction* singleton{ };
		if ( singleton == nullptr )
			singleton = new Prediction;

		return *singleton;
	}
};