#pragma once

#include "../interface/se_interface.h"
#include "../misc/se_def.h"

using namespace vmt_indexes;

#define EngineClient std::get<Address>( VEngineClient ).to<void*>( )
#define ModelInfoClient std::get<Address>( VModelInfoClient ).to<void*>( )

class Engine {
private:
	Interface VEngineClient{ },
		VModelInfoClient{ },
		VEngineModel{ },
		GAMEEVENTSMANAGER{ };
public:

	inline Engine( ) :
		VEngineClient{ Interfaces::inst( )[ STR( "VEngineClient" ) ] },
		VModelInfoClient{ Interfaces::inst( )[ STR( "VModelInfoClient" ) ] },
		VEngineModel{ Interfaces::inst( )[ STR( "VEngineModel" ) ] },
		GAMEEVENTSMANAGER{ Interfaces::inst( ).get( STR( "GAMEEVENTSMANAGER" ), 1 ) } {




	}

	// VEngineClient 
	bool InGame( ) {
		static Vmt vmt( EngineClient );
		return vmt.get<bool( _tc* )( void* )>( VEngineClient::InGame )( EngineClient );
	}
	bool Connected( ) {
		static Vmt vmt( EngineClient );
		return vmt.get<bool( _tc* )( void* )>( VEngineClient::Connected )( EngineClient );
	}
	int Local( ) {
		static Vmt vmt( EngineClient );
		return vmt.get<int( _tc* )( void* )>( VEngineClient::Local )( EngineClient );
	}
	float LastTimeStamp( ) {
		static Vmt vmt( EngineClient );
		return vmt.get<float( _tc* )( void* )>( VEngineClient::LastTimeStamp )( EngineClient );
	}
	int GetPlayerForUserId( int uid ) {
		static Vmt vmt( EngineClient );
		return vmt.get<bool( _tc* )( void*, int )>( VEngineClient::GetPlayerForUserID )( EngineClient, uid );
	}
	void Dimensions( int& a, int& b ) {
		static Vmt vmt( EngineClient );
		return vmt.get<void( _tc* )( void*, int&, int& )>( VEngineClient::Dimensions )( EngineClient, a, b );
	}
	void Info( int idx, PlayerInfo* nfo ) {
		static Vmt vmt( EngineClient );
		return vmt.get<void( _tc* )( void*, int, PlayerInfo* )>( VEngineClient::Info )( EngineClient, idx, nfo );
	}
	void SetAngles( Vector3& in ) {
		static Vmt vmt( EngineClient );
		return vmt.get<void( _tc* )( void*, Vector3& )>( VEngineClient::SetAngles )( EngineClient, in );
	}
	void GetAngles( Vector3& in ) {
		static Vmt vmt( EngineClient );
		return vmt.get<void( _tc* )( void*, Vector3& )>( VEngineClient::GetAngles )( EngineClient, in );
	}
	void ExecuteCmd( char* cmd ) {
		static Vmt vmt( EngineClient );
		return vmt.get<void( _tc* )( void*, char* )>( VEngineClient::ExecuteCmd )( EngineClient, cmd );
	}
	const Matrix4x4& GetViewMatrix( ) {
		static Vmt vmt( EngineClient );
		return vmt.get<const Matrix4x4&( _tc* )( void* )>( VEngineClient::GetViewMatrix )( EngineClient );
	}

	studiohdr_t* GetStudioModel( model_t* mdl ) {
		static Vmt vmt( ModelInfoClient );
		return vmt.get<studiohdr_t*( _tc* ) ( void*, model_t* )>( IVModelInfoClient::GetStudioModel )( ModelInfoClient, mdl );
	}

	inline bool toScr( Vector3 &origin, Vector3 &screen ) {
		auto vmtx = GetViewMatrix( );
		int scrw{ }, scrh{ };
		Dimensions( scrw, scrh );

		float w = vmtx.m( 3, 0 ) * origin[ 0 ] + vmtx.m( 3, 1 ) * origin[ 1 ] + vmtx.m( 3, 2 ) * origin[ 2 ] + vmtx.m( 3, 3 );
		screen[ 2 ] = 0;

		if ( w <= 0 )
			return false;

		float mw = 1 / w;
		screen[ 0 ] = ( scrw * 0.5 ) + ( 0.5 * ( ( vmtx.m( 0, 0 ) * origin[ 0 ] + vmtx.m( 0, 1 ) * origin[ 1 ] + vmtx.m( 0, 2 ) * origin[ 2 ] + vmtx.m( 0, 3 ) ) * mw ) * scrw + 0.5 );
		screen[ 1 ] = ( scrh * 0.5 ) - ( 0.5 * ( ( vmtx.m( 1, 0 ) * origin[ 0 ] + vmtx.m( 1, 1 ) * origin[ 1 ] + vmtx.m( 1, 2 ) * origin[ 2 ] + vmtx.m( 1, 3 ) ) * mw ) * scrh + 0.5 );

		if ( screen[ 0 ] < -( scrw * 0.1 ) || screen[ 0 ] > scrw + ( scrw * 0.1 ) || screen[ 1 ] < -( scrh * 0.1 ) || screen[ 1 ] > scrh + ( scrh * 0.1 ) )
			return false;

		return true;
	}

	// Singleton
	static Engine& inst( ) {
		static Engine* singleton{ };
		if ( singleton == nullptr )
			singleton = new Engine( );

		return *singleton;
	}
};