#pragma once

#include "../interface/se_interface.h"
#include "../misc/se_def.h"
#include "../entity/se_entity.h"

using Mask = uint32_t;
using Vector3 = Vector < 3>;
using Vector4 = Vector<4>;

struct Plane_t {
	Vector3	m_normal;
	float	m_dist;
	uint8_t	m_type;
	uint8_t	m_signbits;
	uint8_t	__pad00[ 0x2 ];
};

struct Surface_t {
	const char* m_name;
	int16_t m_surfaceProps;
	uint16_t m_flags;
};

struct Trace_t {
	Vector3 m_start;
	Vector3 m_end;
	Plane_t m_plane;
	float m_fraction;
	int32_t m_contents;
	uint16_t m_dispFlags;
	bool m_allSolid;
	bool m_startSolid;
};

struct SETrace : Trace_t {
	bool DidHitWorld( ) const;
	bool DidHitNonWorldEntity( ) const;
	int	GetEntityIndex( ) const;
	bool DidHit( ) const;

	float m_fractionleftsolid;
	Surface_t m_surface;

	int32_t m_hitgroup;
	int16_t m_physicsbone;
	uint16_t  m_worldSurfaceIndex;

	Entity* m_entity;

	int32_t m_hitbox;
	uint8_t __pad00[ 0x24 ];
};

class IFilter {
public:
	virtual bool ShouldHitEntity( Entity* ent, Mask mask ) = 0;
	virtual int GetTraceType( ) const = 0;
};

class CFilter : public IFilter {
public:
	bool ShouldHitEntity( Entity* ent, Mask mask ) { return !( ent == m_skip ); }
	virtual int GetTraceType( ) const { return 3; }
	Entity* m_skip;
};

class SEFilter : public CFilter {
public:
	virtual int GetTraceType( ) const { return 3; }
	bool ShouldHitEntity( Entity *ent, Mask mask ) {
		return ent != m_skip && ent != m_wep;
	}
	Entity* m_skip{ },
		*m_wep{ };
};

struct Ray {
	Vector4 m_start{ }, 
		m_delta{ },
		m_offset{ },
		m_extents{ };
#if _CSGO
	uint8_t __pad00[ sizeof( uintptr_t ) ];
#endif

	bool m_isRay{ },
		m_isSwept{ };

	uint8_t __pad01[ 0x4 ];

	inline void init( Vector3 a, Vector3 b ) {
		m_start = Vector4( a[0], a[1], a[2], 0 );
		m_delta = Vector4( b[ 0 ], b[ 1 ], b[ 2 ], 0 ) - Vector4( a[ 0 ], a[ 1 ], a[ 2 ], 0 );
		m_isRay = m_isSwept = true;
	}
	inline Ray( Vector3 a, Vector3 b ) {
		init( a, b );
	}

};

using namespace vmt_indexes;

class Trace {
private:
	Interface EngineTraceClient{ };
public:

	inline Trace( ) :
		EngineTraceClient( Interfaces::inst( )[ STR( "EngineTraceClient" ) ] ) 
	{

	}

	// EngineTraceClient
	inline void TraceRay( const Ray& ray, Mask mask, SEFilter* filt, SETrace& tr ) {
		static Vmt vmt( std::get<Address>( EngineTraceClient ) );
		return vmt.get<void( _tc* )( void*, const Ray&, Mask, SEFilter*, SETrace& )>( EngineTraceClient::TraceRay )( std::get<Address>( EngineTraceClient ), ray, mask, filt, tr );
	}

	inline auto& traceClient( ) {
		return std::get<Address>( EngineTraceClient );
	}

	inline SETrace trace( const Vector3& start, const Vector3& end ) {
		SETrace retn{ };
		Ray ray{ start, end };
		SEFilter filt{ };
		filt.m_skip = Client::localPly( );
		TraceRay( ray, 0x4600400B, &filt, retn );
		return retn;
	}

	inline float fraction( const Vector3& start, const Vector3& end ) {
		SETrace tr{ trace( start, end ) };
		return tr.m_fraction;
	}
	inline auto entity( const Vector3& start, const Vector3& end ) {
		SETrace tr{ trace( start, end ) };
		return tr.m_entity;
	}

	// main isvisible function lolz
	inline  bool isVisible( const Vector3& start, const Vector3& end, const Entity* ent = nullptr ) {
		SETrace tr{ trace( start, end ) };

		if ( ent ) {
			return tr.m_entity == ent || tr.m_fraction == 1.0f;
		}
		else
			return tr.m_fraction == 1.0f;
	}

	static Trace& inst( ) {
		static Trace* singleton{ };
		if ( singleton == nullptr )
			singleton = new Trace( );

		return *singleton;
	}
};