#pragma once

#include "..\main\se_inc.h"

using Interface = std::tuple<Address, Hash, std::string>;

class Interfaces {
private:
	using interfaceFn = void*(*)( );

	struct interfaceReg {
		interfaceFn m_Fn{ };
		const char* m_pName{ };
		interfaceReg* m_pNext{ };
	};

	std::vector<Interface> m_Entries{ };
public:
	inline Interfaces( ) : m_Entries{ } {
		Address createInterface{ };
		interfaceReg* list{ };

		auto peb = Peb::inst( );

		if ( !peb )
			return;

		if ( peb->modules( ).empty( ) )
			return;

		for ( auto& mod : peb->modules( ) ) {
			Address createInterface = mod.exp( STR( "CreateInterface" ) );
			if ( !createInterface )
				continue;

			if ( createInterface.at<opcode>( 0x4 ) == 0xE9 ) { // displaced jump - take it
				createInterface += createInterface.at( 0x5 );
				if ( createInterface.at<opcode>( 0xE ) == 0x35 ) {
					list = Address( createInterface + 0xF ).get<interfaceReg*>( 2 );
					createEntries( list );
				}
				else { 
					// todo - mite never happen?
				}
			}
			else if ( createInterface.at<opcode>( 0x2 ) == 0x35 ) {
				list = Address( createInterface + 0x3 ).get<interfaceReg*>( 2 );
				createEntries( list );
			}
		}
	}

	inline void createEntries( interfaceReg* l ) {
		for ( ; l; l=l->m_pNext ) {
			std::string tmp( l->m_pName );
			tmp.resize( tmp.size( ) - 3 );
			m_Entries.push_back( Interface( l->m_Fn( ), tmp, l->m_pName ) );		
		}
	}

	Interface& operator[] ( Hash hash ) {
		for ( auto& e : m_Entries ) {
			if ( std::get<Hash>( e ) == hash.m_Data )
				return e;
		}
		return m_Entries.front( );
	}

	Interface& get( Hash hash, size_t which = 0 ) {
		for ( auto& e : m_Entries ) {
			if ( std::get<Hash>( e ) == hash.m_Data && which ) {
				which--;
				continue;
			}

			if ( std::get<Hash>( e ) == hash )
				return e;
		}
		return m_Entries.front( );
	}

	std::vector<Interface> interfaces( ) { return m_Entries; }

	static Interfaces& inst( ) {
		static Interfaces* singleton{ };
		if ( singleton == nullptr )
			singleton = new Interfaces( );

		return *singleton;
	}

	static void dump( const std::string& path ) {
		std::ofstream	file{ };

		if ( path.empty( ) )
			return;

		file.open( path.c_str( ), std::ofstream::out | std::ofstream::binary | std::ofstream::trunc );
		if ( !file.good( ) ) {
			file.close( );
			return;
		}

		auto getTime = [ ] ( ) {
			static char		buf[ 26 ]{ };
			std::time_t		time{ };
			struct tm*		timeinfo{ };

			std::time( &time );
			timeinfo = std::localtime( &time );
			std::strftime( buf, sizeof( buf ), STR( "%I:%M%p - %D" ), timeinfo );

			return std::string( buf );
		};

		file << STR( "Dumping " ) << Module( Module::static_indexes::self ).m_Name << STR( " interfaces at " ) << getTime( ) << std::endl << std::endl;

		for ( auto& interf : inst( ).interfaces( ) ) {
			file << std::setw( 48 )
				<< std::setfill( ' ' )
				<< std::left
				<< " " + std::get<std::string>( interf ) + ": "
				<< std::right
				<< std::hex
				<< " 0x"
				<< std::setw( 8 )
				<< std::setfill( '0' )
				<< std::uppercase
				<< std::get<Address>( interf ).to<uintptr_t>( ) << std::endl;
		}
	}
};