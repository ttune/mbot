#pragma once

#include "../client/se_client.h"

struct RecvProp_t;

struct DVariant {
	union {
		float		Float;
		long		Int;
		char*		String;
		void*		Data;
		Vector3		Vec;
		uint64_t	Int64;
	};
	int	m_nType;
};

struct RecvProxyData_t {
	RecvProp_t*		m_pRecvProp;
	DVariant		m_dvValue;
	int32_t			m_nElement, m_nObjectID;
};

struct RecvTable_t {
	RecvProp_t* m_pProps;
	int32_t		m_nProps;
	uintptr_t	m_pDecoder;
	char*		m_pNetTableName;
	uint8_t		__pad00;
};

struct RecvProp_t {	
	using RecvVarProxy = void( *)( const RecvProxyData_t*, void*, void* );

	char*			m_pVarName;
	int32_t			m_nType, m_nFlags, m_nStringLength;
	bool			m_bInsideArray;
	uintptr_t		m_pExtraData;
	RecvProp_t*		m_pArrayProp;
	uintptr_t		m_pArrayLengthProxy;
	RecvVarProxy	m_RecvVarFunction;
	uintptr_t		m_pDataTableFunction;
	RecvTable_t*	m_pDataTable;
	int32_t			m_nOffset, m_nElementStride, m_nElements;
	char*			m_pParrentArrayPropName;
};

struct ClientClass_t {
	uint8_t			__pad00[ 8 ];
	char*			m_pNetworkName;
	RecvTable_t*	m_pRecvTable;
	ClientClass_t*	m_pNext;
	ClientClass		m_nClassId;
};

namespace Netvars {
	enum dt_tables {
		DT_BasePlayer = 0x801EE124, /* DT_BasePlayer */
		DT_CSPlayer = 0x72822E7F, /* DT_CSPlayer */
		DT_BaseCombatWeapon = 0xE977EFF7, /* DT_BaseCombatWeapon */
		DT_BaseCombatCharacter = 0xBC62981A, /* DT_BaseCombatCharacter */
		DT_BaseAttributableItem = 0xA6703EE9, /* DT_BaseAttributableItem */
		DT_BaseAnimating = 0xBF9A8BCF, /* DT_BaseAnimating */
		DT_AI_BaseNPC = 0xBD369DE1, /* DT_AI_BaseNPC */
		DT_BaseEntity = 0x66A47C54 /* DT_BaseEntity */
	};

	static Address find( RecvTable_t* t, Hash prop ) {
		Address offset{ }, extra{ };
		for ( int32_t it{ }; it < t->m_nProps; it++ ) {
			RecvProp_t* p = &t->m_pProps[ it ];
			if ( !p || p->m_pVarName[ 0 ] == '0' )
				continue;

			if ( p->m_pDataTable ) {
				extra = find( p->m_pDataTable, prop );
				if ( extra ) {
					offset = p->m_nOffset + extra;
				}
			}

			if ( Hash( p->m_pVarName ) == prop ) {
				offset = p->m_nOffset + extra;
#if _SE_DEBUG
				static int vvo{ };
				if ( prop == 0x42AA3499 /* m_vecViewOffset[0] */ )  // fucking hack... fix 
					vvo++;
				
				if ( prop == 0x42AA3499 /* m_vecViewOffset[0] */ && vvo > 1 )
					continue;

				io::w_printf( STR( "%s - 0x%X\n" ), p->m_pVarName, offset );

#endif
			}
		}
		return offset;
	}

	static Address netvar( Hash table, Hash prop ) {
		static ClientClass_t* cl{ };
		if ( cl == nullptr )
			cl = Client::inst( ).getClientClass( );

		if ( cl == nullptr )
			return{ };

		do {
			if ( Hash( cl->m_pRecvTable->m_pNetTableName ) == table ) {
#if _SE_DEBUG
				io::w_printf( STR( "[Netvars] %s - " ), cl->m_pRecvTable->m_pNetTableName );
#endif
				return find( cl->m_pRecvTable, prop );
			}
		} while ( cl = cl->m_pNext );

		return{ };
	}

	static void dumpIDs( const std::string& path ) {
		ClientClass_t	*clientClass{ Client::inst( ).getClientClass( ) };
		std::ofstream	file{ };

		if ( path.empty( ) || !clientClass )
			return;

		// open file.
		file.open( path.c_str( ), std::ofstream::out | std::ofstream::binary | std::ofstream::trunc );
		if ( !file.good( ) ) {
			file.close( );
			return;
		}

		auto getTime = [ ] ( ) {
			static char		buf[ 26 ]{ };
			std::time_t		time{ };
			struct tm*		timeinfo{ };

			std::time( &time );
			timeinfo = std::localtime( &time );
			std::strftime( buf, sizeof( buf ), STR( "%I:%M%p - %D" ), timeinfo );

			return std::string( buf );
		};

		file << STR( "Dumping " ) << Module( Module::static_indexes::self ).m_Name << STR( " class IDs at " ) << getTime( ) << std::endl << std::endl;
		
		file << STR( "enum class ClientClass {" ) << std::endl;

		for ( clientClass; clientClass != 0; clientClass = clientClass->m_pNext ) {
			// print class name.
			file << "\t" << clientClass->m_pNetworkName << " = " << ( int32_t )clientClass->m_nClassId << "," << std::endl;
		}

		file << "};" << std::endl;

		file.close( );

	}

	static void dump( const std::string& path ) { // credits d3x, yetiyet
		ClientClass_t	*clientClass{ Client::inst( ).getClientClass( ) };
		std::ofstream	file{ };

		if ( path.empty( ) || !clientClass )
			return;

		// open file.
		file.open( path.c_str( ), std::ofstream::out | std::ofstream::binary | std::ofstream::trunc );
		if ( !file.good( ) ) {
			file.close( );
			return;
		}

		auto getTime = [ ] ( ) {
			static char		buf[ 26 ]{ };
			std::time_t		time{ };
			struct tm*		timeinfo{ };

			std::time( &time );
			timeinfo = std::localtime( &time );
			std::strftime( buf, sizeof( buf ), STR( "%I:%M%p - %D" ), timeinfo );

			return std::string( buf );
		};

		file << STR( "Dumping " ) << Module( Module::static_indexes::self ).m_Name << STR( " netvars at " ) << getTime( ) << std::endl << std::endl;

		// i don't feel like having this as it's own function.
		std::function< size_t( RecvTable_t * ) > dumpTable = [ &dumpTable, &file ] ( RecvTable_t *table ) -> size_t {
			RecvProp_t	*prop{ };
			size_t		offset{ };

			if ( !table )
				return 0;

			for ( int i{ }; i < table->m_nProps; i++ ) {
				// current prop.
				prop = &table->m_pProps[ i ];
				if ( !prop )
					continue;

				// we're just going to skip netvars that contain only numbers for names.
				if ( prop->m_pVarName[ 0 ] == '0' || prop->m_pVarName[ 0 ] == '1' )
					continue;

				// do recursion if needed.
				offset = ( prop->m_pDataTable != 0 ) ? ( dumpTable( prop->m_pDataTable ) + prop->m_nOffset ) : ( prop->m_nOffset );

				// print info for current prop.
				std::string propName( prop->m_pVarName );
				std::stringstream ss{ };

				file << std::setw( 48 )
					<< std::setfill( ' ' )
					<< std::left
					<< " " + propName + ": "
					<< std::right
					<< std::hex
					<< " 0x"
					<< std::setw( 8 )
					<< std::setfill( '0' )
					<< std::uppercase
					<< offset << std::endl;
			}

			return offset;
		};

		for ( clientClass; clientClass != 0; clientClass = clientClass->m_pNext ) {
			// print class name.
			file << "[ " << clientClass->m_pRecvTable->m_pNetTableName << " ]" << std::endl;

			// iterate this table's props and print to file.
			dumpTable( clientClass->m_pRecvTable );

			file << std::endl;
		}

		file.close( );
	}
};