#pragma once

#include "../interface/se_interface.h"
#include "../misc/se_def.h"

#if defined(_CSGO) || defined(_INSURGENCY)  || defined(_GMOD) || defined(_ORANGEBOX)
struct SEColor {
	uint8_t r{ },
		g{ },
		b{ },
		a{ };

	inline SEColor( int r, int g, int b, int a ) :
		r( r ),
		g( g ),
		b( b ),
		a( a ) {
	}
};
#endif

using namespace vmt_indexes;

#define IPanel std::get<Address>( VGUI_Panel ).to<void*>( )
#define ISurface std::get<Address>( VGUI_Surface ).to<void*>( )

class Vgui {
private:
	Interface VGUI_Panel{ },
		VGUI_Surface{ };

public:
	using VPanel = uint32_t;
	using VFont = ULONG;

	inline Vgui( ) :
		VGUI_Panel{ Interfaces::inst( )[ STR( "VGUI_Panel" ) ] },
		VGUI_Surface{ Interfaces::inst( )[ STR( "VGUI_Surface" ) ] }
	{
#if _SE_DEBUG
		io::w_printf( "[Vgui] Panel ... 0x%X\n", IPanel );
		io::w_printf( "[Vgui] Surface ... 0x%X\n", ISurface );
#endif
	
	}

	// IPanel 
	auto GetName( VPanel p ) {
		static Vmt vmt( IPanel );
		return vmt.get<const char*( _tc* ) ( void*, VPanel )>( VGUI_Panel::GetName )( IPanel, p );
	}

	// ISurface
#if _GMOD || _ORANGEBOX
	inline void DrawSetColor( SEColor col ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, int, int, int, int )>( VGUI_Surface::DrawSetColor )( ISurface, col.r, col.g, col.b, col.a );
	}
#else
	inline void DrawSetColor( SEColor col ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, SEColor )>( VGUI_Surface::DrawSetColor )( ISurface, col );
	}
#endif;
	inline void DrawFilledRect( int x0, int y0, int x1, int y1 ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, int, int, int, int )>( VGUI_Surface::DrawFilledRect )( ISurface, x0, y0, x1, y1 );
	}
	inline void DrawOutlinedRect( int x0, int y0, int x1, int y1 ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, int, int, int, int )>( VGUI_Surface::DrawOutlinedRect )( ISurface, x0, y0, x1, y1 );
	}
	inline void DrawLine( int x0, int y0, int x1, int y1 ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, int, int, int, int )>( VGUI_Surface::DrawLine )( ISurface, x0, y0, x1, y1 );
	}
	inline void DrawSetTextFont( VFont font ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, VFont )>( VGUI_Surface::DrawSetTextFont )( ISurface, font );
	}
#if _GMOD
	inline void DrawSetTextColor( SEColor col ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, int, int, int, int )>( VGUI_Surface::DrawSetTextColor )( ISurface, col.r, col.g, col.b, col.a );
	}
#else
	inline void DrawSetTextColor( SEColor col ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, SEColor )>( VGUI_Surface::DrawSetTextColor )( ISurface, col );
}
#endif
	inline void DrawSetTextPos( int x, int y ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, int, int )>( VGUI_Surface::DrawSetTextPos )( ISurface, x, y );
	}
	inline void DrawPrintText( const wchar_t *text, int textLen ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, const wchar_t *, int, int )>( VGUI_Surface::DrawPrintText )( ISurface, text, textLen, 0 );
	}
	inline VFont CreateFont( ) {
		static Vmt vmt( ISurface );
		return vmt.get<VFont( _tc* )( void* )>( VGUI_Surface::CreateFont )( ISurface );
	}
	inline bool SetFontGlyphSet( VFont &font, const char *windowsfont, int tall, int weight, int blur, int scanlines, int flags ) {
		static Vmt vmt( ISurface );
		return vmt.get<bool( _tc* )( void*, VFont, const char*, int, int, int, int, int, int, int )>( VGUI_Surface::SetFontGlyphSet )( ISurface, font, windowsfont, tall, weight, blur, scanlines, flags, 0, 0 );
	}
	inline void GetTextSize( VFont font, const wchar_t *text, int &w, int &h ) {
		static Vmt vmt( ISurface );
		return vmt.get<void( _tc* )( void*, VFont, const wchar_t*, int &, int & )>( VGUI_Surface::GetTextSize )( ISurface, font, text, w, h );
	}

	inline bool canDraw( VPanel pnl ) {
		static Vgui::VPanel panelDesired{ };
		static uint64_t call{ };

#if _GMOD
		const Hash hash = STR( "OverlayPopupPanel" );
#else
		const Hash hash = STR( "FocusOverlayPanel" );
#endif
		if ( !panelDesired ) {
			if ( Hash( Vgui::inst( ).GetName( pnl ) ) == hash )
				panelDesired = pnl;
			else
				return false;
		}

		if ( panelDesired != pnl )
			return false;

#if _CSGO
		return ( call++ % 2 == 0 );
#else
		return true;
#endif
	}

	Address getPanel( ) {
		return IPanel;
	}
	Address getSurface( ) {
		return ISurface;
	}

	static Vgui& inst( ) {
		static Vgui* singleton{ };
		if ( singleton == nullptr )
			singleton = new Vgui( );

		return *singleton;
	}

};