#pragma once

#include <m_irenderer.h>
#include "../vgui/se_vgui.h"
#include "../engine/se_engine.h"

class Render : public IRender {
private:
	struct Fontdata_t {
		Fontdata_t( std::string name = "", int size = 12 ) : m_size( size ), m_name( name ) { }
		int m_size = 12;
		std::string m_name{ };
		Vgui::VFont m_font{ };
	};

	std::array<Fontdata_t, 3> m_fonts{
		Fontdata_t( STR( "Tahoma" ) ),
		Fontdata_t( STR( "Arial" ) ),
		Fontdata_t( STR( "Fixedsys" ), 16 )
	};

public:
	inline Render( ) {
		for ( auto& f : m_fonts ) {
			f.m_font = Vgui::inst( ).CreateFont( );
			Vgui::inst( ).SetFontGlyphSet( f.m_font, f.m_name.c_str( ), f.m_size, 500, 0, 0, FONTFLAG_ANTIALIAS | FONTFLAG_DROPSHADOW );
		}
	}

	virtual void DrawSpace( int& w, int& h ) {
		return Engine::inst( ).Dimensions( w, h );
	}

	virtual void Filled( int x, int y, int w, int h, Color col ) {
		Vgui::inst( ).DrawSetColor( makeColor( col ) );
		Vgui::inst( ).DrawFilledRect( x, y, x + w, y + h );
	}

	virtual void WText( int x, int y, Color col, Font font, Align align, wchar_t * buf ) {
		Vgui::inst( ).DrawSetTextFont( m_fonts[ font ].m_font );
		Vgui::inst( ).DrawSetTextColor( makeColor( col ) );
		int wide = 0, tall = 0;

		if ( align == Align::center ) Vgui::inst( ).GetTextSize( m_fonts[ font ].m_font, buf, wide, tall ); //center
		if ( align == Align::right ) {
			Vgui::inst( ).GetTextSize( m_fonts[ font ].m_font, buf, wide, tall );
			wide *= 2;
		}
		Vgui::inst( ).DrawSetTextPos( ( int ) ( x - ( wide * .5 ) ), y );
		Vgui::inst( ).DrawPrintText( buf, ( int ) wcslen( buf ) );
	}

	virtual void Line( int x1, int y1, int x2, int y2, Color col ) {
		Vgui::inst( ).DrawSetColor( makeColor( col ) );
		Vgui::inst( ).DrawLine( x1, y1, x2, y2 );
	}

	inline SEColor makeColor( Color col ) {
		return *( SEColor* ) ( &col );
	}

	static Render& inst( ) {
		static Render* singleton{ };
		if ( singleton == nullptr )
			singleton = new Render( );

		return *singleton;
	}
};
