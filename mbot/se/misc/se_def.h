#pragma once

#include "../main/se_inc.h"

#ifdef CreateFont
#undef CreateFont
#endif

using Vector3 = Vector<3>;
using Matrix4x4 = Matrix<float, 4, 4>;
using Matrix3x4 = Matrix<float, 3, 4>;

using RandomSeed = void( *)( uint32_t );
using RandomFloat = float( *)( float, float );

using MD5_PseudoRandom = uint32_t( *)( uint32_t );

#define	FL_ONGROUND		(1<<0)
#define FL_DUCKING		(1<<1)
#define	FL_WATERJUMP	(1<<2)
#define FL_ONTRAIN		(1<<3)
#define FL_INRAIN		(1<<4)
#define FL_FROZEN		(1<<5)
#define FL_ATCONTROLS	(1<<6)
#define	FL_CLIENT		(1<<7)
#define FL_FAKECLIENT	(1<<8)
#define	FL_INWATER		(1<<9)

namespace vmt_indexes {
	// do stuff with dif games here

	////////////////
	//
	// CLIENT
	namespace VClient {
#if defined(_CSGO) || defined(_INSURGENCY) || defined(_GMOD) || defined(_ORANGEBOX)
		const uint16_t GetClientClass = 8;
		const uint16_t WriteUsercmdDeltaToBuffer = 25;
		const uint16_t CreateMove = 21;
		const uint16_t FrameStageNotify = 35;
#endif 
	}
	namespace VClientEntityList {
#if defined(_CSGO) || defined(_INSURGENCY) || defined(_GMOD) || defined(_ORANGEBOX)
		const uint16_t GetEntityByHandle = 4;
		const uint16_t GetHighestEntityIndex = 6;
#endif 
	}
	namespace IClientMode {
#if _CSGO
		const uint16_t CreateMove = 24;
#elif _GMOD || _ORANGEBOX
		const uint16_t CreateMove = 21;
#endif
	}
	namespace IClientEntity {
#if _CSGO
		const uint16_t GetAbsOrigin = 10;
		const uint16_t GetAbsAngles = 11;
		const uint16_t GetClientClass = 2;
		const uint16_t GetRenderBounds = 17;
		const uint16_t SetupBones = 13;
#elif _INSURGENCY
		const uint16_t GetAbsOrigin = 10;
		const uint16_t GetAbsAngles = 11;
		const uint16_t GetClientClass = 2;
		const uint16_t GetRenderBounds = 17;
#elif _GMOD|| _ORANGEBOX
		const uint16_t GetAbsOrigin = 9;
		const uint16_t GetAbsAngles = 10;
		const uint16_t GetClientClass = 2;
		const uint16_t GetRenderBounds = 20;
		const uint16_t GetModel = 9;
		const uint16_t SetupBones = 16;
#endif
	}
	namespace GameMovement {
		const uint16_t InPrediction = 14;

#if defined(_CSGO) || defined(_GMOD)
		const uint16_t ProcessMovement = 1;
#endif
	}
	namespace VClientPrediction {
#if _CSGO
		const uint16_t SetupMove = 20;
		const uint16_t FinishMove = 21;
#elif _GMOD
		const uint16_t SetupMove = 18;
		const uint16_t FinishMove = 19;
#endif
	}
	namespace IMoveHelper {
#if _CSGO
		const uint16_t SetHost = 1;
#endif
	}

	////////////////
	//
	// ENGINE
	namespace VEngineClient {	
#if defined(_CSGO) || defined(_INSURGENCY) || defined(_GMOD) || defined(_ORANGEBOX)
		const uint16_t Dimensions = 5;
		const uint16_t Info = 8;
		const uint16_t GetPlayerForUserID = 9; 
		const uint16_t Local = 12;
		const uint16_t LastTimeStamp = 14;
		const uint16_t InGame = 26;
		const uint16_t Connected = 27;
#endif
#if _INSURGENCY
		const uint16_t GetAngles = 17;
		const uint16_t SetAngles = 19;
		const uint16_t GetViewMatrix = 39;
		const uint16_t ExecuteCmd = 107;
#elif _CSGO
		const uint16_t GetAngles = 18;
		const uint16_t SetAngles = 19;		
		const uint16_t GetViewMatrix = 37;
		const uint16_t ExecuteCmd = 108;
#elif _GMOD || _ORANGEBOX
		const uint16_t GetAngles = 19;
		const uint16_t SetAngles = 20;
		const uint16_t GetViewMatrix = 36;
#endif
#if _GMOD
		const uint16_t ExecuteCmd = 106;
#elif _ORANGEBOX
		const uint16_t ExecuteCmd = 102;
#endif
	}

	namespace IVModelInfoClient {
#if _CSGO
		const uint16_t GetModelIndex = 2;
		const uint16_t GetModelName = 3;
		const uint16_t GetStudioModel = 30;
		const uint16_t GetModelMaterials = 17;
#elif _ORANGEBOX
		const uint16_t GetStudioModel = 28;
#endif
	}

	namespace IInput {
#if defined(_CSGO) || defined(_INSURGENCY) || defined(_GMOD) || defined(_ORANGEBOX)		
		const uint16_t GetUsercmd = 8;
#endif

#if defined(_CSGO) || defined(_INSURGENCY) || defined(_GMOD)
		const uint16_t WriteUsercmdDeltaToBuffer = 6;
#elif _ORANGEBOX
		const uint16_t WriteUsercmdDeltaToBuffer = 5;

		
		
#endif
	}
	namespace EngineTraceClient {
#if defined(_CSGO) || defined(_INSURGENCY)
		const uint16_t TraceRay = 5;
#elif _GMOD || _ORANGEBOX
		const uint16_t TraceRay = 4;
		const uint16_t GetPointContents = 0;
#endif
	}
	namespace VModelInfoClient {
#if defined(_CSGO) || defined(_INSURGENCY)
		const uint16_t GetStudioModel = 30;
#endif
	}


	////////////////
	//
	// VGUI
	namespace VGUI_Surface {
#if defined(_CSGO) || defined(_INSURGENCY)
		const uint16_t DrawFilledRect = 16;
		const uint16_t DrawOutlinedRect = 18;
		const uint16_t DrawLine = 19;
		const uint16_t GetTextSize = 79;
#elif _GMOD || _ORANGEBOX
		const uint16_t DrawFilledRect = 12;
		const uint16_t DrawLine = 15;
#endif
#if _GMOD
		const uint16_t DrawOutlinedRect = 14;
		const uint16_t GetTextSize = 76;
#elif _ORANGEBOX
		const uint16_t DrawOutlinedRect = 13;
		const uint16_t GetTextSize = 75;
#endif		
#if _CSGO
		const uint16_t DrawSetColor = 14;
		const uint16_t DrawSetTextFont = 23;
		const uint16_t DrawSetTextColor = 24;
		const uint16_t DrawSetTextPos = 26;
		const uint16_t DrawPrintText = 28;
		const uint16_t CreateFont = 71;
		const uint16_t SetFontGlyphSet = 72;
#elif _INSURGENCY
		const uint16_t DrawSetColor = 13;
		const uint16_t DrawSetTextFont = 22;
		const uint16_t DrawSetTextColor = 23;
		const uint16_t DrawSetTextPos = 25;
		const uint16_t DrawPrintText = 27;
		const uint16_t CreateFont = 70;
		const uint16_t SetFontGlyphSet = 71;
#elif _GMOD || _ORANGEBOX
		const uint16_t DrawSetColor = 11;
		const uint16_t DrawSetTextFont = 17;
		const uint16_t DrawSetTextPos = 20;
		const uint16_t DrawPrintText = 22;
		const uint16_t CreateFont = 66;
		const uint16_t SetFontGlyphSet = 67;
#endif
#if _GMOD
		const uint16_t DrawSetTextColor = 19;
#elif _ORANGEBOX
		const uint16_t DrawSetTextColor = 18;
#endif
	}
	namespace VGUI_Panel {
#if defined(_CSGO) || defined(_INSURGENCY)  || defined(_GMOD) || defined(_ORANGEBOX)
		const uint16_t GetName = 36;
		const uint16_t PaintTraverse = 41;
#endif	
	}

}

struct Globals {
	float	m_flRealTime;
	int		m_nFramecount;
	float	m_flAbsoluteFrameTime;
#if _CSGO
	uint8_t	__pad00[ 4 ];
#endif
	float	m_flCurTime;
	float	m_flFrameTime;
	int		m_nMaxClients;
	int		m_nTickCount;
	float	m_flIntervalPerTick;
	float	m_flInerpolationAmount;
};
struct PlayerInfo
{
#if _CSGO
	uint8_t __pad00[ 0x8 ];
	int32_t m_xuidLow;
	int32_t m_xuidHigh;
	char m_name[ 128 ];
	int32_t	 m_userID;
	char m_steamID[ 32 ];
	uint8_t __pad03[ 500 ];
#elif _INSURGENCY
	uint8_t __pad00[ 0x8 ];
	char m_name[ 32 ];
	int32_t m_userID;
	char m_guid[ 33 ];
	uint32_t m_friendsID;
	char m_friendsName[ 32 ];
	bool m_fakeplayer;
	bool m_ishltv;
	uint8_t __pad01[ 25 ];
#elif _GMOD || _ORANGEBOX
	int8_t			m_name[ 32 ];
	int32_t			m_userID;
	int8_t			m_guid[ 33 ];
	uint32_t		m_friendsID;
	int8_t			m_friendsName[ 32 ];
	bool			m_fakeplayer;
	bool			m_ishltv;
	ULONG			m_customFiles[ 4 ];
	uint8_t			m_filesDownloaded;
#endif
};
struct Cmd {
	uint8_t __pad00[ sizeof( uintptr_t ) ]; // vmt
	int32_t m_cmdNum;
	int32_t m_tickCount;

	Vector3 m_viewAngles;
#if _CSGO
	Vector3 m_aimDirection;
#endif
	Vector3 m_move;

	int32_t m_Buttons;

	uint8_t m_Impulse;

	int32_t m_weaponSelect;
	int32_t m_weaponSubtype;
	int32_t m_randomSeed;

	int16_t m_mousedX;
	int16_t m_mousedY;

	bool m_bHasBeenPredicted;
#if _CSGO
	uint8_t __pad01[ 0x18 ];
#elif _INSURGENCY
	Vector3 m_freeAimAngles;
	Vector3 m_muzzleAngles;
	Vector3 m_headAngles;
	Vector3 m_headOffset;
#endif
};

enum buttons {
	attack = 1,
	jump = 2,
	duck = 4,
	use = 32,
	attack2 = 2048,
	reload = 8192,
};

using model_t = uint8_t;
struct mstudiobone_t
{
	int			sznameindex;
	int			parent;
	int			bonecontroller[ 6 ];
	Vector3		pos;
	float		quat[ 4 ];
	Vector3		rot;
	Vector3		posscale;
	Vector3		rotscale;
	Matrix3x4	poseToBone;
	float		qAlignment[ 4 ];
	int			flags;
	int			proctype;
	int			procindex;
	mutable int	physicsbone;
	int			surfacepropidx;
	int			contents;
	int			unused[ 8 ];
	const char* GetSurfaceProp( void ) const
	{
		return ( ( char* )this ) + surfacepropidx;
	}

	void* GetProcedure( void ) const
	{
		if ( !procindex ) return nullptr;
		return ( void* ) ( ( ( byte* )this ) + procindex );
	}
	const char* GetName( void ) const
	{
		return ( ( char* )this ) + sznameindex;
	}
};

struct mstudiobbox_t
{
	int		bone;
	int		group;
	Vector3	bbmin;
	Vector3	bbmax;
	int		szhitboxnameindex;
	int		unused[ 8 ];
	const char* GetHitboxName( void ) const
	{
		if ( !szhitboxnameindex ) return "";
		return ( ( char* )this ) + szhitboxnameindex;
	}
};

struct mstudiohitboxset_t
{
	int	sznameindex;
	int	numhitboxes;
	int	hitboxindex;
	mstudiobbox_t* GetHitbox( int i ) const
	{
		return ( mstudiobbox_t* ) ( ( ( byte* )this ) + hitboxindex ) + i;
	}
	const char* GetName( void ) const
	{
		return ( ( char* )this ) + sznameindex;
	}
};

struct studiohdr_t
{
	int			id;
	int			version;
	long		checksum;
	char		name[ 64 ];
	int			length;
	Vector3		eyeposition;
	Vector3		illumposition;
	Vector3		hull_min;
	Vector3		hull_max;
	Vector3		view_bbmin;
	Vector3		view_bbmax;
	int			flags;
	int			numbones;
	int			boneindex;
	int			numbonecontrollers;
	int			bonecontrollerindex;
	int			numhitboxsets;
	int			hitboxsetindex;
	int			numlocalanim;
	int			localanimindex;
	int			numlocalseq;
	int			localseqindex;
	mutable int	activitylistversion;
	mutable int	eventsindexed;
	int			numtextures;
	int			textureindex;
	int GetHitboxCount( int set ) const
	{
		mstudiohitboxset_t* pSet = GetHitboxSet( set );
		if ( !pSet ) return 0;
		return pSet->numhitboxes;
	}
	mstudiohitboxset_t* GetHitboxSet( int i ) const
	{
		return ( mstudiohitboxset_t* ) ( ( ( byte* )this ) + hitboxsetindex ) + i;
	}

	mstudiobbox_t* GetHitbox( int i, int set ) const
	{
		mstudiohitboxset_t* pSet = GetHitboxSet( set );
		if ( !pSet ) return nullptr;
		return pSet->GetHitbox( i );
	}
	mstudiobone_t* GetBone( int i ) const
	{
		return ( mstudiobone_t* ) ( ( ( byte* )this ) + boneindex ) + i;
	}
	const char* GetName( void ) const
	{
		return name;
	}
};

enum class ClientClass {
#if _CSGO
	CTestTraceline = 194,
	CTEWorldDecal = 195,
	CTESpriteSpray = 192,
	CTESprite = 191,
	CTESparks = 190,
	CTESmoke = 189,
	CTEShowLine = 187,
	CTEProjectedDecal = 184,
	CFEPlayerDecal = 61,
	CTEPlayerDecal = 183,
	CTEPhysicsProp = 180,
	CTEParticleSystem = 179,
	CTEMuzzleFlash = 178,
	CTELargeFunnel = 176,
	CTEKillPlayerAttachments = 175,
	CTEImpact = 174,
	CTEGlowSprite = 173,
	CTEShatterSurface = 186,
	CTEFootprintDecal = 170,
	CTEFizz = 169,
	CTEExplosion = 167,
	CTEEnergySplash = 166,
	CTEEffectDispatch = 165,
	CTEDynamicLight = 164,
	CTEDecal = 162,
	CTEClientProjectile = 161,
	CTEBubbleTrail = 160,
	CTEBubbles = 159,
	CTEBSPDecal = 158,
	CTEBreakModel = 157,
	CTEBloodStream = 156,
	CTEBloodSprite = 155,
	CTEBeamSpline = 154,
	CTEBeamRingPoint = 153,
	CTEBeamRing = 152,
	CTEBeamPoints = 151,
	CTEBeamLaser = 150,
	CTEBeamFollow = 149,
	CTEBeamEnts = 148,
	CTEBeamEntPoint = 147,
	CTEBaseBeam = 146,
	CTEArmorRicochet = 145,
	CTEMetalSparks = 177,
	CSteamJet = 140,
	CSmokeStack = 133,
	DustTrail = 244,
	CFireTrail = 64,
	SporeTrail = 250,
	SporeExplosion = 249,
	RocketTrail = 247,
	SmokeTrail = 248,
	CPropVehicleDriveable = 120,
	ParticleSmokeGrenade = 246,
	CParticleFire = 99,
	MovieExplosion = 245,
	CTEGaussExplosion = 172,
	CEnvQuadraticBeam = 56,
	CEmbers = 46,
	CEnvWind = 60,
	CPrecipitation = 114,
	CPrecipitationBlocker = 115,
	CBaseTempEntity = 18,
	NextBotCombatCharacter = 0,
	CEconWearable = 45,
	CBaseAttributableItem = 4,
	CEconEntity = 44,
	CWeaponXM1014 = 242,
	CWeaponTaser = 237,
	CSmokeGrenade = 131,
	CWeaponSG552 = 234,
	CSensorGrenade = 127,
	CWeaponSawedoff = 230,
	CWeaponNOVA = 226,
	CIncendiaryGrenade = 87,
	CMolotovGrenade = 96,
	CWeaponM3 = 218,
	CKnifeGG = 93,
	CKnife = 92,
	CHEGrenade = 84,
	CFlashbang = 66,
	CWeaponElite = 209,
	CDecoyGrenade = 40,
	CDEagle = 39,
	CWeaponUSP = 241,
	CWeaponM249 = 217,
	CWeaponUMP45 = 240,
	CWeaponTMP = 239,
	CWeaponTec9 = 238,
	CWeaponSSG08 = 236,
	CWeaponSG556 = 235,
	CWeaponSG550 = 233,
	CWeaponScout = 232,
	CWeaponSCAR20 = 231,
	CSCAR17 = 125,
	CWeaponP90 = 229,
	CWeaponP250 = 228,
	CWeaponP228 = 227,
	CWeaponNegev = 225,
	CWeaponMP9 = 224,
	CWeaponMP7 = 223,
	CWeaponMP5Navy = 222,
	CWeaponMag7 = 221,
	CWeaponMAC10 = 220,
	CWeaponM4A1 = 219,
	CWeaponHKP2000 = 216,
	CWeaponGlock = 215,
	CWeaponGalilAR = 214,
	CWeaponGalil = 213,
	CWeaponG3SG1 = 212,
	CWeaponFiveSeven = 211,
	CWeaponFamas = 210,
	CWeaponBizon = 205,
	CWeaponAWP = 203,
	CWeaponAug = 202,
	CAK47 = 1,
	CWeaponCSBaseGun = 207,
	CWeaponCSBase = 206,
	CC4 = 29,
	CWeaponBaseItem = 204,
	CBaseCSGrenade = 8,
	CSmokeGrenadeProjectile = 132,
	CSensorGrenadeProjectile = 128,
	CMolotovProjectile = 97,
	CItem_Healthshot = 91,
	CDecoyProjectile = 41,
	CFireCrackerBlast = 62,
	CInferno = 88,
	CChicken = 31,
	CFootstepControl = 68,
	CCSGameRulesProxy = 34,
	CWeaponCubemap = 0,
	CWeaponCycler = 208,
	CTEPlantBomb = 181,
	CTEFireBullets = 168,
	CTERadioIcon = 185,
	CPlantedC4 = 107,
	CCSTeam = 38,
	CCSPlayerResource = 36,
	CCSPlayer = 35,
	CCSRagdoll = 37,
	CTEPlayerAnimEvent = 182,
	CHostage = 85,
	CHostageCarriableProp = 86,
	CBaseCSGrenadeProjectile = 9,
	CHandleTest = 83,
	CTeamplayRoundBasedRulesProxy = 144,
	CSpriteTrail = 138,
	CSpriteOriented = 137,
	CSprite = 136,
	CRagdollPropAttached = 123,
	CRagdollProp = 122,
	CPredictedViewModel = 116,
	CPoseController = 112,
	CGameRulesProxy = 82,
	CInfoLadderDismount = 89,
	CFuncLadder = 74,
	CTEFoundryHelpers = 171,
	CEnvDetailController = 52,
	CWorld = 243,
	CWaterLODControl = 201,
	CWaterBullet = 200,
	CVoteController = 199,
	CVGuiScreen = 198,
	CPropJeep = 119,
	CPropVehicleChoreoGeneric = 0,
	CTriggerSoundOperator = 197,
	CBaseVPhysicsTrigger = 22,
	CTriggerPlayerMovement = 196,
	CBaseTrigger = 20,
	CTest_ProxyToggle_Networkable = 193,
	CTesla = 188,
	CBaseTeamObjectiveResource = 17,
	CTeam = 143,
	CSunlightShadowControl = 142,
	CSun = 141,
	CParticlePerformanceMonitor = 100,
	CSpotlightEnd = 135,
	CSpatialEntity = 134,
	CSlideshowDisplay = 130,
	CShadowControl = 129,
	CSceneEntity = 126,
	CRopeKeyframe = 124,
	CRagdollManager = 121,
	CPhysicsPropMultiplayer = 105,
	CPhysBoxMultiplayer = 103,
	CPropDoorRotating = 118,
	CBasePropDoor = 16,
	CDynamicProp = 43,
	CProp_Hallucination = 117,
	CPostProcessController = 113,
	CPointCommentaryNode = 111,
	CPointCamera = 110,
	CPlayerResource = 109,
	CPlasma = 108,
	CPhysMagnet = 106,
	CPhysicsProp = 104,
	CStatueProp = 139,
	CPhysBox = 102,
	CParticleSystem = 101,
	CMovieDisplay = 98,
	CMaterialModifyControl = 95,
	CLightGlow = 94,
	CInfoOverlayAccessor = 90,
	CFuncTrackTrain = 81,
	CFuncSmokeVolume = 80,
	CFuncRotating = 79,
	CFuncReflectiveGlass = 78,
	CFuncOccluder = 77,
	CFuncMoveLinear = 76,
	CFuncMonitor = 75,
	CFunc_LOD = 70,
	CTEDust = 163,
	CFunc_Dust = 69,
	CFuncConveyor = 73,
	CFuncBrush = 72,
	CBreakableSurface = 28,
	CFuncAreaPortalWindow = 71,
	CFish = 65,
	CFireSmoke = 63,
	CEnvTonemapController = 59,
	CEnvScreenEffect = 57,
	CEnvScreenOverlay = 58,
	CEnvProjectedTexture = 55,
	CEnvParticleScript = 54,
	CFogController = 67,
	CEnvDOFController = 53,
	CCascadeLight = 30,
	CEnvAmbientLight = 51,
	CEntityParticleTrail = 50,
	CEntityFreezing = 49,
	CEntityFlame = 48,
	CEntityDissolve = 47,
	CDynamicLight = 42,
	CColorCorrectionVolume = 33,
	CColorCorrection = 32,
	CBreakableProp = 27,
	CBeamSpotlight = 25,
	CBaseButton = 5,
	CBaseToggle = 19,
	CBasePlayer = 15,
	CBaseFlex = 12,
	CBaseEntity = 11,
	CBaseDoor = 10,
	CBaseCombatCharacter = 6,
	CBaseAnimatingOverlay = 3,
	CBoneFollower = 26,
	CBaseAnimating = 2,
	CAI_BaseNPC = 0,
	CBeam = 24,
	CBaseViewModel = 21,
	CBaseParticleEntity = 14,
	CBaseGrenade = 13,
	CBaseCombatWeapon = 7,
	CBaseWeaponWorldModel = 23
#elif _GMOD
	CTestTraceline = 197,
	CTEWorldDecal = 198,
	CTESpriteSpray = 195,
	CTESprite = 194,
	CTESparks = 193,
	CTESmoke = 192,
	CTEShowLine = 190,
	CTEProjectedDecal = 188,
	CTEPlayerDecal = 187,
	CTEPhysicsProp = 185,
	CTEParticleSystem = 184,
	CTEMuzzleFlash = 183,
	CTELargeFunnel = 181,
	CTEKillPlayerAttachments = 180,
	CTEImpact = 179,
	CTEGlowSprite = 177,
	CTEShatterSurface = 189,
	CTEFootprintDecal = 175,
	CTEFizz = 174,
	CTEExplosion = 173,
	CTEEnergySplash = 172,
	CTEEffectDispatch = 171,
	CTEDynamicLight = 170,
	CTEDecal = 168,
	CTEClientProjectile = 166,
	CTEBubbleTrail = 165,
	CTEBubbles = 164,
	CTEBSPDecal = 163,
	CTEBreakModel = 162,
	CTEBloodStream = 161,
	CTEBloodSprite = 160,
	CTEBeamSpline = 159,
	CTEBeamRingPoint = 158,
	CTEBeamRing = 157,
	CTEBeamPoints = 156,
	CTEBeamLaser = 155,
	CTEBeamFollow = 154,
	CTEBeamEnts = 153,
	CTEBeamEntPoint = 152,
	CTEBaseBeam = 151,
	CTEArmorRicochet = 150,
	CTEMetalSparks = 182,
	CSteamJet = 146,
	CSmokeStack = 141,
	DustTrail = 230,
	CFireTrail = 59,
	SporeTrail = 237,
	SporeExplosion = 236,
	RocketTrail = 234,
	SmokeTrail = 235,
	CPropVehicleDriveable = 127,
	ParticleSmokeGrenade = 233,
	CParticleFire = 104,
	MovieExplosion = 231,
	CTEGaussExplosion = 176,
	CEnvQuadraticBeam = 52,
	CEmbers = 44,
	CEnvWind = 57,
	CPrecipitation = 118,
	CBaseTempEntity = 30,
	NextBotCombatCharacter = 232,
	CWeaponStunStick = 227,
	CWeaponSMG1 = 226,
	CWeapon_SLAM = 206,
	CWeaponShotgun = 225,
	CWeaponRPG = 224,
	CLaserDot = 90,
	CWeaponPistol = 223,
	CWeaponPhysCannon = 221,
	CBaseHL2MPCombatWeapon = 24,
	CBaseHL2MPBludgeonWeapon = 23,
	CHL2MPMachineGun = 82,
	CWeaponHL2MPBase = 219,
	CWeaponFrag = 217,
	CWeaponCrowbar = 214,
	CWeaponCrossbow = 213,
	CWeaponAR2 = 209,
	CWeapon357 = 205,
	CHL2MPGameRulesProxy = 81,
	CTEHL2MPFireBullets = 178,
	CTEPlayerAnimEvent = 186,
	CHL2MPRagdoll = 83,
	CHL2MP_Player = 80,
	CCrossbowBolt = 41,
	CWeaponHopwire = 0,
	CWeaponOldManHarpoon = 220,
	CWeaponCitizenSuitcase = 212,
	CWeaponCitizenPackage = 211,
	CWeaponAlyxGun = 207,
	CWeaponCubemap = 215,
	CWeaponGaussGun = 0,
	CWeaponAnnabelle = 208,
	CFlaregun = 0,
	CWeaponBugBait = 210,
	CWeaponBinoculars = 0,
	CWeaponCycler = 216,
	CVortigauntEffectDispel = 201,
	CVortigauntChargeToken = 200,
	CNPC_Vortigaunt = 103,
	CPropVehiclePrisonerPod = 128,
	CPropCrane = 122,
	CPropCannon = 0,
	CPropAirboat = 120,
	CFlare = 61,
	CTEConcussiveExplosion = 167,
	CNPC_Strider = 102,
	CScriptIntro = 135,
	CRotorWashEmitter = 133,
	CPropCombineBall = 121,
	CPlasmaBeamNode = 0,
	CNPC_RollerMine = 101,
	CNPC_Manhack = 99,
	CNPC_CombineGunship = 98,
	CNPC_AntlionGuard = 95,
	CInfoTeleporterCountdown = 89,
	CMortarShell = 94,
	CEnvStarfield = 55,
	CEnvHeadcrabCanister = 49,
	CAlyxEmpEffect = 11,
	CCorpse = 40,
	CCitadelEnergyCore = 37,
	CHL2_Player = 79,
	CBaseHLBludgeonWeapon = 25,
	CHLSelectFireMachineGun = 85,
	CHLMachineGun = 84,
	CBaseHelicopter = 20,
	CNPC_Barney = 97,
	CNPC_Barnacle = 96,
	AR2Explosion = 0,
	CTEAntlionDust = 149,
	CBaseHLCombatWeapon = 26,
	CWeaponHandGrenade = 218,
	CBaseHL1MPCombatWeapon = 22,
	CBaseHL1CombatWeapon = 21,
	CAIWeaponAR2 = 3,
	CAIWeaponShotgun = 8,
	CAIWeaponCrossbow = 4,
	CAIWeaponCrowbar = 5,
	CAIWeaponRPG = 7,
	CAIWeaponSMG1 = 9,
	CAIWeaponPistol = 6,
	CAIWeapon357 = 2,
	CGMOD_Player = 76,
	CAIWeaponStunStick = 10,
	CFleshEffectTarget = 0,
	CPropJeepEpisodic = 124,
	CPropScalable = 125,
	CNPC_Puppet = 100,
	CHandleTest = 78,
	CSpriteTrail = 145,
	CSpriteOriented = 144,
	CSprite = 143,
	CRagdollPropAttached = 131,
	CRagdollProp = 130,
	CPredictedViewModel = 119,
	CPoseController = 117,
	CGameRulesProxy = 75,
	CInfoLadderDismount = 86,
	CFuncLadder = 68,
	CEnvDetailController = 48,
	CWorld = 229,
	CWaterLODControl = 204,
	CWaterBullet = 203,
	CVoteController = 202,
	CVGuiScreen = 199,
	CPropJeep = 123,
	CPropVehicleChoreoGeneric = 126,
	CTest_ProxyToggle_Networkable = 196,
	CTesla = 191,
	CTeam = 148,
	CSun = 147,
	CParticlePerformanceMonitor = 105,
	CSpotlightEnd = 142,
	CSlideshowDisplay = 140,
	CShadowControl = 139,
	CSceneEntity = 134,
	CRopeKeyframe = 132,
	CRagdollManager = 129,
	CPhysicsPropMultiplayer = 111,
	CPhysBoxMultiplayer = 109,
	CBasePropDoor = 29,
	CDynamicProp = 43,
	CPointCommentaryNode = 116,
	CPointCamera = 115,
	CPlayerResource = 114,
	CPlasma = 113,
	CPhysMagnet = 112,
	CPhysicsProp = 110,
	CPhysBox = 108,
	CParticleSystem = 106,
	CMaterialModifyControl = 93,
	CLightGlow = 91,
	CInfoOverlayAccessor = 88,
	CFuncTrackTrain = 74,
	CFuncSmokeVolume = 73,
	CFuncRotating = 72,
	CFuncReflectiveGlass = 71,
	CFuncOccluder = 70,
	CFuncMonitor = 69,
	CFunc_LOD = 65,
	CTEDust = 169,
	CFunc_Dust = 64,
	CFuncConveyor = 67,
	CBreakableSurface = 36,
	CFuncAreaPortalWindow = 66,
	CFish = 60,
	CEntityFlame = 46,
	CFireSmoke = 58,
	CEnvTonemapController = 56,
	CEnvScreenEffect = 53,
	CEnvScreenOverlay = 54,
	CEnvProjectedTexture = 51,
	CEnvParticleScript = 50,
	CFogController = 63,
	CEntityParticleTrail = 47,
	CEntityDissolve = 45,
	CDynamicLight = 42,
	CColorCorrectionVolume = 39,
	CColorCorrection = 38,
	CBreakableProp = 35,
	CBasePlayer = 28,
	CBaseFlex = 18,
	CBaseEntity = 17,
	CBaseDoor = 16,
	CBaseCombatCharacter = 14,
	CBaseAnimatingOverlay = 13,
	CBoneFollower = 33,
	CBaseAnimating = 12,
	CInfoLightingRelative = 87,
	CAI_BaseNPC = 1,
	CBeam = 32,
	CBaseViewModel = 31,
	CBaseParticleEntity = 27,
	CBaseGrenade = 19,
	CBaseCombatWeapon = 15,
	CWeaponPhysGun = 222,
	CPhysBeam = 107,
	CLuaNextBot = 92,
	CFlexManipulate = 62,
	CBoneManipulate = 34,
	CWeaponSWEP = 228,
	CSENT_point = 138,
	CSENT_anim = 137,
	CSENT_AI = 136,
	CGMODGameRulesProxy = 77,
#elif _ORANGEBOX

#if _CSS
	CTestTraceline = 156,
	CTEWorldDecal = 157,
	CTESpriteSpray = 154,
	CTESprite = 153,
	CTESparks = 152,
	CTESmoke = 151,
	CTEShowLine = 149,
	CTEProjectedDecal = 146,
	CTEPlayerDecal = 145,
	CTEPhysicsProp = 142,
	CTEParticleSystem = 141,
	CTEMuzzleFlash = 140,
	CTELargeFunnel = 138,
	CTEKillPlayerAttachments = 137,
	CTEImpact = 136,
	CTEGlowSprite = 135,
	CTEShatterSurface = 148,
	CTEFootprintDecal = 133,
	CTEFizz = 132,
	CTEExplosion = 130,
	CTEEnergySplash = 129,
	CTEEffectDispatch = 128,
	CTEDynamicLight = 127,
	CTEDecal = 125,
	CTEClientProjectile = 124,
	CTEBubbleTrail = 123,
	CTEBubbles = 122,
	CTEBSPDecal = 121,
	CTEBreakModel = 120,
	CTEBloodStream = 119,
	CTEBloodSprite = 118,
	CTEBeamSpline = 117,
	CTEBeamRingPoint = 116,
	CTEBeamRing = 115,
	CTEBeamPoints = 114,
	CTEBeamLaser = 113,
	CTEBeamFollow = 112,
	CTEBeamEnts = 111,
	CTEBeamEntPoint = 110,
	CTEBaseBeam = 109,
	CTEArmorRicochet = 108,
	CTEMetalSparks = 139,
	CSteamJet = 104,
	CSmokeStack = 99,
	DustTrail = 188,
	CFireTrail = 47,
	SporeTrail = 195,
	SporeExplosion = 194,
	RocketTrail = 192,
	SmokeTrail = 193,
	CPropVehicleDriveable = 90,
	ParticleSmokeGrenade = 191,
	CParticleFire = 73,
	MovieExplosion = 189,
	CTEGaussExplosion = 134,
	CEnvQuadraticBeam = 41,
	CEmbers = 34,
	CEnvWind = 45,
	CPrecipitation = 87,
	CBaseTempEntity = 17,
	CWeaponXM1014 = 186,
	CWeaponUSP = 185,
	CWeaponUMP45 = 184,
	CWeaponTMP = 183,
	CSmokeGrenade = 98,
	CWeaponSG552 = 182,
	CWeaponSG550 = 181,
	CWeaponScout = 180,
	CWeaponP90 = 179,
	CWeaponP228 = 178,
	CWeaponMP5Navy = 177,
	CWeaponMAC10 = 176,
	CWeaponM4A1 = 175,
	CWeaponM3 = 174,
	CWeaponM249 = 173,
	CKnife = 70,
	CHEGrenade = 65,
	CWeaponGlock = 172,
	CWeaponGalil = 171,
	CWeaponG3SG1 = 170,
	CFlashbang = 49,
	CWeaponFiveSeven = 169,
	CWeaponFamas = 168,
	CWeaponElite = 167,
	CDEagle = 31,
	CWeaponCSBaseGun = 165,
	CWeaponCSBase = 164,
	CC4 = 23,
	CBaseCSGrenade = 6,
	CWeaponAWP = 163,
	CWeaponAug = 162,
	CAK47 = 1,
	NextBotCombatCharacter = 190,
	CFootstepControl = 51,
	CCSGameRulesProxy = 26,
	CWeaponCubemap = 0,
	CWeaponCycler = 166,
	CTEPlantBomb = 143,
	CTEFireBullets = 131,
	CTERadioIcon = 147,
	CPlantedC4 = 81,
	CCSTeam = 30,
	CCSPlayerResource = 28,
	CCSPlayer = 27,
	CCSRagdoll = 29,
	CTEPlayerAnimEvent = 144,
	CHostage = 66,
	CBaseCSGrenadeProjectile = 7,
	CHandleTest = 64,
	CTeamplayRoundBasedRulesProxy = 107,
	CSpriteTrail = 103,
	CSpriteOriented = 102,
	CSprite = 101,
	CRagdollPropAttached = 93,
	CRagdollProp = 92,
	CPredictedViewModel = 88,
	CPoseController = 86,
	CGameRulesProxy = 63,
	CInfoLadderDismount = 67,
	CFuncLadder = 56,
	CEnvDetailController = 38,
	CWorld = 187,
	CWaterLODControl = 161,
	CWaterBullet = 160,
	CVoteController = 159,
	CVGuiScreen = 158,
	CPropJeep = 89,
	CPropVehicleChoreoGeneric = 0,
	CTest_ProxyToggle_Networkable = 155,
	CTesla = 150,
	CTeamTrainWatcher = 0,
	CBaseTeamObjectiveResource = 16,
	CTeam = 106,
	CSun = 105,
	CParticlePerformanceMonitor = 74,
	CSpotlightEnd = 100,
	CSlideshowDisplay = 97,
	CShadowControl = 96,
	CSceneEntity = 95,
	CRopeKeyframe = 94,
	CRagdollManager = 91,
	CPhysicsPropMultiplayer = 79,
	CPhysBoxMultiplayer = 77,
	CBasePropDoor = 15,
	CDynamicProp = 33,
	CPointCommentaryNode = 85,
	CPointCamera = 84,
	CPlayerResource = 83,
	CPlasma = 82,
	CPhysMagnet = 80,
	CPhysicsProp = 78,
	CPhysBox = 76,
	CParticleSystem = 75,
	CMaterialModifyControl = 72,
	CLightGlow = 71,
	CInfoOverlayAccessor = 69,
	CFuncTrackTrain = 62,
	CFuncSmokeVolume = 61,
	CFuncRotating = 60,
	CFuncReflectiveGlass = 59,
	CFuncOccluder = 58,
	CFuncMonitor = 57,
	CFunc_LOD = 53,
	CTEDust = 126,
	CFunc_Dust = 52,
	CFuncConveyor = 55,
	CBreakableSurface = 22,
	CFuncAreaPortalWindow = 54,
	CFish = 48,
	CEntityFlame = 36,
	CFireSmoke = 46,
	CEnvTonemapController = 44,
	CEnvScreenEffect = 42,
	CEnvScreenOverlay = 43,
	CEnvProjectedTexture = 40,
	CEnvParticleScript = 39,
	CFogController = 50,
	CEntityParticleTrail = 37,
	CEntityDissolve = 35,
	CDynamicLight = 32,
	CColorCorrectionVolume = 25,
	CColorCorrection = 24,
	CBreakableProp = 21,
	CBasePlayer = 13,
	CBaseFlex = 10,
	CBaseEntity = 9,
	CBaseDoor = 8,
	CBaseCombatCharacter = 4,
	CBaseAnimatingOverlay = 3,
	CBoneFollower = 20,
	CBaseAnimating = 2,
	CInfoLightingRelative = 68,
	CAI_BaseNPC = 0,
	CBeam = 19,
	CBaseViewModel = 18,
	CBaseProjectile = 14,
	CBaseParticleEntity = 12,
	CBaseGrenade = 11,
	CBaseCombatWeapon = 5,
#elif _HL2
	CTestTraceline = 167,
	CTEWorldDecal = 168,
	CTESpriteSpray = 165,
	CTESprite = 164,
	CTESparks = 163,
	CTESmoke = 162,
	CTEShowLine = 160,
	CTEProjectedDecal = 158,
	CTEPlayerDecal = 157,
	CTEPhysicsProp = 156,
	CTEParticleSystem = 155,
	CTEMuzzleFlash = 154,
	CTELargeFunnel = 152,
	CTEKillPlayerAttachments = 151,
	CTEImpact = 150,
	CTEGlowSprite = 149,
	CTEShatterSurface = 159,
	CTEFootprintDecal = 147,
	CTEFizz = 146,
	CTEExplosion = 145,
	CTEEnergySplash = 144,
	CTEEffectDispatch = 143,
	CTEDynamicLight = 142,
	CTEDecal = 140,
	CTEClientProjectile = 138,
	CTEBubbleTrail = 137,
	CTEBubbles = 136,
	CTEBSPDecal = 135,
	CTEBreakModel = 134,
	CTEBloodStream = 133,
	CTEBloodSprite = 132,
	CTEBeamSpline = 131,
	CTEBeamRingPoint = 130,
	CTEBeamRing = 129,
	CTEBeamPoints = 128,
	CTEBeamLaser = 127,
	CTEBeamFollow = 126,
	CTEBeamEnts = 125,
	CTEBeamEntPoint = 124,
	CTEBaseBeam = 123,
	CTEArmorRicochet = 122,
	CTEMetalSparks = 153,
	CSteamJet = 118,
	CSmokeStack = 113,
	DustTrail = 194,
	CFireTrail = 46,
	SporeTrail = 200,
	SporeExplosion = 199,
	RocketTrail = 197,
	SmokeTrail = 198,
	CPropVehicleDriveable = 102,
	ParticleSmokeGrenade = 196,
	CParticleFire = 82,
	MovieExplosion = 195,
	CTEGaussExplosion = 148,
	CEnvQuadraticBeam = 39,
	CEmbers = 31,
	CEnvWind = 44,
	CPrecipitation = 95,
	CBaseTempEntity = 18,
	CHalfLife2Proxy = 62,
	CWeaponStunStick = 192,
	CWeaponPhysCannon = 187,
	CCrossbowBolt = 28,
	CWeaponCrowbar = 183,
	CWeapon_SLAM = 0,
	CWeaponCrossbow = 182,
	CWeapon357 = 175,
	CWeaponSMG1 = 191,
	CWeaponShotgun = 190,
	CWeaponPistol = 188,
	CWeaponRPG = 189,
	CWeaponFrag = 186,
	CWeaponAR2 = 178,
	CWeaponCitizenSuitcase = 181,
	CWeaponCitizenPackage = 180,
	CWeaponAlyxGun = 176,
	CWeaponCubemap = 184,
	CWeaponGaussGun = 0,
	CWeaponAnnabelle = 177,
	CFlaregun = 0,
	CWeaponBugBait = 179,
	CWeaponBinoculars = 0,
	CWeaponCycler = 185,
	CVortigauntEffectDispel = 171,
	CVortigauntChargeToken = 170,
	CNPC_Vortigaunt = 81,
	CPropVehiclePrisonerPod = 103,
	CPropCrane = 99,
	CPropCannon = 97,
	CPropAirboat = 96,
	CFlare = 48,
	CTEConcussiveExplosion = 139,
	CNPC_Strider = 80,
	CScriptIntro = 110,
	CRotorWashEmitter = 108,
	CPropCombineBall = 98,
	CPlasmaBeamNode = 0,
	CNPC_RollerMine = 79,
	CNPC_Manhack = 78,
	CNPC_CombineGunship = 77,
	CNPC_AntlionGuard = 74,
	CInfoTeleporterCountdown = 70,
	CMortarShell = 73,
	CEnvStarfield = 42,
	CEnvHeadcrabCanister = 36,
	CAlyxEmpEffect = 2,
	CCorpse = 27,
	CCitadelEnergyCore = 24,
	CHL2_Player = 64,
	CBaseHLBludgeonWeapon = 12,
	CHLSelectFireMachineGun = 66,
	CHLMachineGun = 65,
	CBaseHelicopter = 11,
	CNPC_Barney = 76,
	CNPC_Barnacle = 75,
	AR2Explosion = 0,
	CTEAntlionDust = 121,
	CBaseHLCombatWeapon = 13,
	CHandleTest = 63,
	CTeamplayRoundBasedRulesProxy = 0,
	CSpriteTrail = 117,
	CSpriteOriented = 116,
	CSprite = 115,
	CRagdollPropAttached = 106,
	CRagdollProp = 105,
	CPoseController = 94,
	CGameRulesProxy = 61,
	CInfoLadderDismount = 67,
	CFuncLadder = 54,
	CEnvDetailController = 35,
	CWorld = 193,
	CWaterLODControl = 174,
	CWaterBullet = 173,
	CVoteController = 172,
	CVGuiScreen = 169,
	CPropJeep = 100,
	CPropVehicleChoreoGeneric = 101,
	CTest_ProxyToggle_Networkable = 166,
	CTesla = 161,
	CBaseTeamObjectiveResource = 0,
	CTeam = 120,
	CSun = 119,
	CParticlePerformanceMonitor = 83,
	CSpotlightEnd = 114,
	CSlideshowDisplay = 112,
	CShadowControl = 111,
	CSceneEntity = 109,
	CRopeKeyframe = 107,
	CRagdollManager = 104,
	CPhysicsPropMultiplayer = 88,
	CPhysBoxMultiplayer = 86,
	CBasePropDoor = 17,
	CDynamicProp = 30,
	CPointCommentaryNode = 93,
	CPointCamera = 92,
	CPlayerResource = 91,
	CPlasma = 90,
	CPhysMagnet = 89,
	CPhysicsProp = 87,
	CPhysBox = 85,
	CParticleSystem = 84,
	CMaterialModifyControl = 72,
	CLightGlow = 71,
	CInfoOverlayAccessor = 69,
	CFuncTrackTrain = 60,
	CFuncSmokeVolume = 59,
	CFuncRotating = 58,
	CFuncReflectiveGlass = 57,
	CFuncOccluder = 56,
	CFuncMonitor = 55,
	CFunc_LOD = 51,
	CTEDust = 141,
	CFunc_Dust = 50,
	CFuncConveyor = 53,
	CBreakableSurface = 23,
	CFuncAreaPortalWindow = 52,
	CFish = 47,
	CEntityFlame = 33,
	CFireSmoke = 45,
	CEnvTonemapController = 43,
	CEnvScreenEffect = 40,
	CEnvScreenOverlay = 41,
	CEnvProjectedTexture = 38,
	CEnvParticleScript = 37,
	CFogController = 49,
	CEntityParticleTrail = 34,
	CEntityDissolve = 32,
	CDynamicLight = 29,
	CColorCorrectionVolume = 26,
	CColorCorrection = 25,
	CBreakableProp = 22,
	CBasePlayer = 15,
	CBaseFlex = 9,
	CBaseEntity = 8,
	CBaseDoor = 7,
	CBaseCombatCharacter = 5,
	CBaseAnimatingOverlay = 4,
	CBoneFollower = 21,
	CBaseAnimating = 3,
	CInfoLightingRelative = 68,
	CAI_BaseNPC = 1,
	CBeam = 20,
	CBaseViewModel = 19,
	CBaseProjectile = 16,
	CBaseParticleEntity = 14,
	CBaseGrenade = 10,
	CBaseCombatWeapon = 6,
#endif

#endif
};


enum EFontFlags {
	FONTFLAG_NONE,
	FONTFLAG_ITALIC = 0x001,
	FONTFLAG_UNDERLINE = 0x002,
	FONTFLAG_STRIKEOUT = 0x004,
	FONTFLAG_SYMBOL = 0x008,
	FONTFLAG_ANTIALIAS = 0x010,
	FONTFLAG_GAUSSIANBLUR = 0x020,
	FONTFLAG_ROTARY = 0x040,
	FONTFLAG_DROPSHADOW = 0x080,
	FONTFLAG_ADDITIVE = 0x100,
	FONTFLAG_OUTLINE = 0x200,
	FONTFLAG_CUSTOM = 0x400
};