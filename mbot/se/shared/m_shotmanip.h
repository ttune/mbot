#pragma once

#include "../client/se_client.h"

class ShotManip {
private:
	Vector3 m_shotDir{ },
		m_right{ },
		m_up{ },
		m_result{ };
public:

	inline ShotManip( const Vector3& fwd ) {
		setShootDir( fwd );
	}

	inline void setShootDir( const Vector3& fwd ) {
		m_shotDir = fwd;
		m_shotDir.vectorVectors( m_right, m_up );
	}

	inline const Vector3& getSpreadDir( Vector3 spread, float bias = 1.f ) {
		float x{ }, y{ }, z{ };

		if ( bias > 1.0 )
			bias = 1.0;
		else if ( bias < 0.0 )
			bias = 0.0;

		float shotBiasMin = -1.f;
		float shotBiasMax = 1.f;

		float shotBias = ( ( shotBiasMax - shotBiasMin ) * bias ) + shotBiasMin;
		float flatness = ( fabsf( shotBias ) * 0.5 );

		do {
			x = Client::inst( ).r_float( -1, 1 ) * flatness + Client::inst( ).r_float( -1, 1 ) * ( 1 - flatness );
			y = Client::inst( ).r_float( -1, 1 ) * flatness + Client::inst( ).r_float( -1, 1 ) * ( 1 - flatness );
			if ( shotBias < 0 ) {
				x = ( x >= 0 ) ? 1.0 - x : -1.0 - x;
				y = ( y >= 0 ) ? 1.0 - y : -1.0 - y;
			}

			z = x*x + y*y;

		} while ( z > 1 );

		m_result = m_shotDir + m_right * x  * spread.v.x  + m_up * y * spread.v.y;

		return m_result;
	}

};