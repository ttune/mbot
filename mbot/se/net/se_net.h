#pragma once

#include "../client/se_client.h"

#define NETMSG_TYPE_BITS	5	// must be 2^NETMSG_TYPE_BITS > SVC_LASTMSG

enum net {
	NOP,
	Disconnect,
	LastControlMessage,
	SplitScreenUser,
	Tick,
	StringCmd,
	SetConVar,
	SignonState
};

enum svc {
	Print = 7,
	ServerInfo,
	SendTable,
	ClassInfo,
	SetPause,
	CreateStrinTabe,
	UpdateStrinTabe,
	VoiceInit,
	VoiceData,
	Sounds = 17,
	SetView,
	FixAngle,
	CrosshairAnle,
	BSPDecal,
	UserMessage = 23,
	EntityMessae,
	GameEvent,
	PacketEntites,
	TempEntitie,
	Prefetch,
	Menu,
	GameEventList,
	GetCvarValue,
	LASTMSG = 31
};

class bf_write
{
public:
	ULONG* m_pData; //0x0000
	int32_t m_nDataBytes; //0x0004
	int32_t m_nDataBits; //0x0008
	int32_t m_iCurBit; //0x000C
	bool m_bOverflow; //0x0010
	uint8_t m_AssertOnOverflow; //0x0011
	char pad_0012[ 2 ]; //0x0012
	char* m_DebugName; //0x0014

	inline int BitByte( int bits ){
		return ( bits + 7 ) >> 3;
	}
	inline int GetNumBytesWritten( ) {
		return BitByte( m_iCurBit );
	}
	inline int GetNumBitsWritten( ) {
		return m_iCurBit;
	}
	inline int GetMaxNumBits( ) {
		return m_nDataBits;
	}
	inline int GetNumBitsLeft( ) {
		return m_nDataBits - m_iCurBit;
	}
	inline int GetNumBytesLeft( ) {
		return GetNumBitsLeft( ) >> 3;
	}
	inline bool	IsOverflowed( ) const { 
		return m_bOverflow; 
	}
	inline unsigned char* GetData( ) {
		return ( unsigned char* ) m_pData;
	}
	inline const unsigned char* GetData( ) const {
		return ( unsigned char* ) m_pData;
	}
	inline void SetOverflowFlag( ) {
		m_bOverflow = true;
	}

	__forceinline unsigned long LoadLittleDWord( const ULONG *base, ULONG dwordIndex ) {
		return base[ dwordIndex ];
	}
	__forceinline void StoreLittleDWord( ULONG *base, ULONG dwordIndex, ULONG dword ) {
		base[ dwordIndex ] = dword;	
	}

	__forceinline bool CheckForOverflow( int nBits ) {
		if ( m_iCurBit + nBits > m_nDataBits ) {
			SetOverflowFlag( );
		}
		return m_bOverflow;
	}

	inline void WriteUBitLong( unsigned int curData, int numbits, bool bCheckRange = false ) {
		//using fn = int( __thiscall* )( bf_write*, unsigned int, int );
		//static fn f{ };
		//if ( !f ) {
		//	f = Pattern( STR( "client.dll" ), STR( "55 8B EC 53 8B 5D 0C 57 8B F9 8B 4F 0C" ) ).to<fn>( );
		//	io::w_printf( STR( "[Network] WriteUBitLong ... (0x%X)\n" ), f );
		//}
		//else
		//	f( this, curData, numbits );

		/* other 
		static unsigned long BitWriteMasks[ 32 ][ 33 ];

		// Bounds checking..
		if ( ( m_iCurBit + numbits ) > m_nDataBits )
		{
			m_iCurBit = m_nDataBits;
			SetOverflowFlag( );
			return;
		}

		int nBitsLeft = numbits;
		int iCurBit = m_iCurBit;

		// Mask in a dword.
		unsigned int iDWord = iCurBit >> 5;

		unsigned long iCurBitMasked = iCurBit & 31;

		unsigned long dword = LoadLittleDWord( m_pData, iDWord );

		dword &= BitWriteMasks[ iCurBitMasked ][ nBitsLeft ];
		dword |= curData << iCurBitMasked;

		// write to stream (lsb to msb ) properly
		StoreLittleDWord( m_pData, iDWord, dword );

		// Did it span a dword?
		int nBitsWritten = 32 - iCurBitMasked;
		if ( nBitsWritten < nBitsLeft )
		{
			nBitsLeft -= nBitsWritten;
			curData >>= nBitsWritten;

			// read from stream (lsb to msb) properly 
			dword = LoadLittleDWord(  m_pData, iDWord + 1 );

			dword &= BitWriteMasks[ 0 ][ nBitsLeft ];
			dword |= curData;

			// write to stream (lsb to msb) properly 
			StoreLittleDWord(  m_pData, iDWord + 1, dword );
		}

		m_iCurBit += numbits;
		*/

		 //alt 
		if ( GetNumBitsLeft( ) < numbits ) {
			m_iCurBit = m_nDataBits;
			SetOverflowFlag( );
			return;
		}
		
		int iCurBitMasked = m_iCurBit & 31;
		int iDWord = m_iCurBit >> 5;
		m_iCurBit += numbits;
		
		// Mask in a dword.
		ULONG * pOut = &m_pData[ iDWord ];
		
		// Rotate data into dword alignment
		curData = ( curData << iCurBitMasked ) | ( curData >> ( 32 - iCurBitMasked ) );
		
		// Calculate bitmasks for first and second word
		unsigned int temp = 1 << ( numbits - 1 );
		unsigned int mask1 = ( temp * 2 - 1 ) << iCurBitMasked;
		unsigned int mask2 = ( temp - 1 ) >> ( 31 - iCurBitMasked );
		
		// Only look beyond current word if necessary (avoid access violation)
		int i = mask2 & 1;
		unsigned long dword1 = LoadLittleDWord( pOut, 0 );
		unsigned long dword2 = LoadLittleDWord( pOut, i );
		
		// Drop bits into place
		dword1 ^= ( mask1 & ( curData ^ dword1 ) );
		dword2 ^= ( mask2 & ( curData ^ dword2 ) );
		
		// Note reversed order of writes so that dword1 wins if mask2 == 0 && i == 0
		StoreLittleDWord( pOut, i, dword2 );
		StoreLittleDWord( pOut, 0, dword1 );
	
	}
	void WriteSBitLong( int nData, int nNumBits ) {
		WriteUBitLong( ( uint32_t ) nData, nNumBits, false );
	}
	void WriteByte( int val ) {
		WriteUBitLong( val, sizeof( unsigned char ) << 3, false );
	}
	void WriteChar( int val ) {
		WriteSBitLong( val, sizeof( char ) << 3 );
	}

	bool WriteString( const char *pStr ) {
		if ( pStr ) {
			do
			{
				WriteChar( *pStr );
				++pStr;
			} while ( *( pStr - 1 ) != 0 );
		}
		else
		{
			WriteChar( 0 );
		}

		return !IsOverflowed( );
	}

}; //Size: 0x0018

class NetChannel
{
public:
	char pad_0000[ 48 ]; //0x0000
	bf_write m_StreamReliable; //0x0030
	char pad_0048[ 12 ]; //0x0048
	bf_write m_StreamUnrealiable; //0x0054
	char pad_006C[ 12 ]; //0x006C
	bf_write m_StreamVoice; //0x0078

	class NET_SetConvar {
	public:
		// setinfo callback str:
		// "Syntax: setinfo <key> <value>\n"

		// baseclass ctor
		// E8 ? ? ? ? 56 57 8D 4C 24 24

		// ctor 
		// E8 ? ? ? ? A1 ? ? ? ? 8D 54 24 1C

		NET_SetConvar( const char* name, const char* val ) {
			using NET_SetConvar_BaseClass_Ctor = int( __thiscall* )( NET_SetConvar* );
			using NET_SetConvar_Ctor = int( __thiscall* )( NET_SetConvar*, const char*, const char* );

			static NET_SetConvar_BaseClass_Ctor baseCtor{ };
			static NET_SetConvar_Ctor ctor{ };

			if ( !ctor || !baseCtor ) {
				auto ref = Stringref( STR( "engine.dll" ), STR( "Syntax: setinfo <key> <value>\n" ) ); // could shorten scan a bit to start from here

#if _SE_DEBUG
				io::w_printf( STR( "[Network::setinfo] setinfo ref ... (0x%X)\n" ), ref );
#endif // _SE_DEBUG

				baseCtor = Pattern( STR( "engine.dll" ), STR( "E8 ?? ?? ?? ?? 56 57 8D 4C 24 24" ) ).at<NET_SetConvar_BaseClass_Ctor>( 0x1 );
				ctor = Pattern( STR( "engine.dll" ), STR( "E8 ?? ?? ?? ?? A1 ?? ?? ?? ?? 8D 54 24 1C" ) ).at<NET_SetConvar_Ctor>( 0x1 );

#if _SE_DEBUG
				io::w_printf( STR( "[Network::NET_SetConvar] BaseClass_Ctor ... (0x%X)\n" ), baseCtor );
				io::w_printf( STR( "[Network::NET_SetConvar] Ctor ... (0x%X)\n" ), ctor );
#endif // _SE_DEBUG
			}

			//baseCtor( this );
			//ctor( this, name, val );
		}
	};

	bool SendNetMsg( NET_SetConvar& msg, bool bForceReliable = false, bool bVoice = false ) {
		static Vmt vmt( this );
		return vmt.get<bool( __thiscall* )( NetChannel*, NET_SetConvar&, bool, bool )>( 42 )( this, msg, bForceReliable, bVoice );
	}
}; //Size: 0x0090

class Network {
private:
	NetChannel* m_NetChannel{ };
	bf_write* m_Buf{ };
public:

	inline Network( ) : 
		m_NetChannel{ Client::inst( ).getClientstate( ).at( 0x94 ).get<NetChannel*>( ) }
	{
#if _SE_DEBUG
		io::w_printf( STR( "[Network] INetChannel ... (0x%X)\n" ), m_NetChannel );
#endif // _SE_DEBUG
	}

	inline void writeName( const char* pName ) {
		m_Buf = &m_NetChannel->m_StreamReliable;
		m_Buf->WriteUBitLong( net::SetConVar, NETMSG_TYPE_BITS );
		m_Buf->WriteByte( 1 );
		m_Buf->WriteString( "name" );
		m_Buf->WriteString( pName );
	}

	void writeName2( const char* pName ) {
		NetChannel::NET_SetConvar convar( "name", pName );
		m_NetChannel->SendNetMsg( convar );
	}

	static Network& inst( ) {
		static Network* singleton{ };
		if ( singleton == nullptr )
			singleton = new Network( );

		return *singleton;
	}
};