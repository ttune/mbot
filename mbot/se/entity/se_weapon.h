#pragma once

#include "../entity/se_entity.h"

#if _CSGO
enum class wepDefinition {
	none,
	deagle,
	elite,
	fiveseven,
	glock,
	ak47 = 7,
	aug,
	awp,
	famas,
	g3sg1,
	galilar = 13,
	m249,
	m4a4 = 16,
	mac10,
	p90 = 19,
	ump45 = 24,
	xm1014,
	bizon,
	mag7,
	negev,
	sawedoff,
	tec9,
	taser,
	p2000,
	mp7,
	mp9,
	nova,
	p250,
	scar20 = 38,
	sg553,
	ssg08,
	knife_ct = 42,
	flashbang,
	hegrenade,
	smokegrenade,
	molotov,
	decoy,
	incgrenade,
	c4,
	knife_t = 59,
	m4a1s = 60,
	usp = 61,
	cz75 = 63,
	revolver = 64,
	bayonet = 500,
	flip = 505,
	gut,
	karambit,
	m9bayonet,
	huntsman,
	falchion = 512,
	bowie = 514,
	butterfly = 515,
	daggers = 516
};
#endif

class Weapon : public Entity {
private:
public:

#if _CSGO
	inline wepDefinition m_iItemDefinitionIndex( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BaseCombatWeapon, 0xE443550A /* m_iItemDefinitionIndex */ );
		return nv<wepDefinition>( v );
	}
#endif

	inline float m_flNextPrimaryAttack( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BaseCombatWeapon, 0xDF43C07E /* m_flNextPrimaryAttack */ );
		return nv<float>( v );
	}
	inline int32_t m_iClip1( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BaseCombatWeapon, 0x9E7E9373 /* m_iClip1 */ );
		return nv<int32_t>( v );
	}
	inline Handle m_hOwner( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BaseCombatWeapon, 0x9D08AD24 /* m_hOwner */ );
		return nv<Handle>( v );
	}

	inline Vector3 getSpread( int32_t seed ) {
		Vector3 out{ };
		float rx{ }, ry{ };

		if ( getBulletSpread( ).empty( ) )
			return Vector3{ };

		Client::inst( ).r_seed( seed & 0xFF );

		rx = Client::inst( ).r_float( -.5f, .5f ) + Client::inst( ).r_float( -.5f, .5f );
		ry = Client::inst( ).r_float( -.5f, .5f ) + Client::inst( ).r_float( -.5f, .5f );

		return Vector3(
			getBulletSpread( )[ 0 ] * rx,
			getBulletSpread( )[ 1 ] * ry,
			0.f
		);
	}

#if _HL2 || _GMOD
	inline Vector3 getBulletSpread( ) {
#elif
	inline float& getBulletSpread( ){
#endif
#if _GMOD
		return spread( );
#elif _HL2
		using getSpreadFn = Vector3&( _tc* )( Weapon* );
		static uint32_t vmtIndex{ };
		static Vector3 nullvec{ };

		if ( !vmtIndex ) {
			auto scan = Pattern( STR( "client.dll" ), STR( "FF 92 ?? ?? ?? ?? 8D 4D 98" ) );
			if ( !scan ) {
				io::w_printf( STR( "[Weapon] cannot find GetSpread index!\n" ) );
				return nullvec;
			}
			else {
				vmtIndex = scan.at<uint32_t>( 0x2 ) / sizeof( uintptr_t );
				io::w_printf( STR( "[Weapon] GetSpread ... %d!\n" ), vmtIndex );
			}		
		}
			
		if ( vmtIndex ) {
			return Vmt::vfn<getSpreadFn>( this, vmtIndex )( this );
		}
		else return nullvec;
#endif
	}

	inline Entity* getOwner( ) {
		return Client::inst( ).entity( m_hOwner( ), true );
	}

#if _CSS || _CSGO
	inline bool isKnife( ) {
		if ( getClass( ) ) {
			if ( getClass( )->m_nClassId == ClientClass::CKnife )
				return true;
		}
		return false;
	}
#endif

	inline static Vector3& spread( ) {
		static Vector3* bulletSpread{ };
		if ( !bulletSpread )
			bulletSpread = new Vector3;
		
		return *bulletSpread;
	}

	inline std::string getName( ) {
		ClientClass_t* cl{ getClass( ) };
		if ( !cl )
			return "";

		std::string retn = cl->m_pNetworkName;
		if ( retn.find( STR( "CWeapon" ) ) != std::string::npos )
			retn.erase( 0, 7 );
		else if ( retn[ 0 ] == 'C' )
			retn.erase( 0, 1 );

#if _CSGO || _ORANGEBOX
		if ( retn.find( STR( "DEagle" ) ) != std::string::npos )
			retn[ 1 ] = 'e';
#endif

		return retn;
	}

};