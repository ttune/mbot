#pragma once

#include "../netvars/se_netvars.h"

using namespace Netvars;

enum class lifestate_t : uint8_t {
	alive,
	dying,
	dead
};

class Weapon;
class Entity {
protected:
	template<typename ret> inline ret nv( Address offset ) { return *( ret* ) ( this + offset ); }

	inline void* networkable( ) { return this + 0x8; }
	inline void* renderable( ) { return this + 0x4; }

public:
	inline int m_iHealth( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BaseCombatCharacter, 0x7977B670 /* m_iHealth */ );
		return nv<int>( v );
	}
	inline int m_fFlags( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BasePlayer, 0x97B54024 /* m_fFlags */ );
		return nv<int>( v );
	}
	inline int m_nTickBase( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BasePlayer, 0x4828F445 /* m_nTickBase */ );
		return nv<int>( v );
	}
	inline int m_iTeamNum( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BaseEntity, 0x44294A51 /* m_iTeamNum */ );
		return nv<int>( v );
	}
	inline int m_nHitboxSet( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BaseCombatCharacter, 0xE24C2FB9 /* m_nHitboxSet */ );
		return nv<int>( v );
	}
	inline float m_flNextAttack( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BasePlayer, 0xEFBA85BA /* m_flNextAttack */ );
		return nv<float>( v );
	}

	inline Handle m_hActiveWeapon( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BasePlayer, 0xDD6C43F /* m_hActiveWeapon */ );
		return nv<Handle>( v );
	}

	inline Vector3 m_vecViewOffset( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BasePlayer, 0x42AA3499 /* m_vecViewOffset[0] */ );
		return nv<Vector3>( v );
	}

	inline Vector3 m_vecPunchAngle( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BasePlayer, 0x54BB6894 /* m_vecPunchAngle */ );
		return nv<Vector3>( v );
	}

	inline lifestate_t m_lifeState( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_AI_BaseNPC, 0xDCBB42B2 /* m_lifeState */ );
		return nv<lifestate_t>( v );
	}

	inline Matrix3x4 m_matCoordinateFrame( ) {
		static Address v{ };
		if ( !v ) v = Netvars::netvar( DT_BasePlayer, 0x97B54024 /* m_fFlags */ ) - 0x48;
		return nv<Matrix3x4>( v );
	}

	// IClientEntity
	Vector3& getAbsOrigin( ) {
		return Vmt::vfn<Vector3&( _tc* ) ( void* )>( this, vmt_indexes::IClientEntity::GetAbsOrigin )( this );
	}
	Vector3& getAbsAngles( ) {
		return Vmt::vfn<Vector3&( _tc* ) ( void* )>( this, vmt_indexes::IClientEntity::GetAbsAngles )( this );
	}

	// Networkable
	ClientClass_t* getClass( ) {
		return Vmt::vfn<ClientClass_t*( _tc* ) ( void* )>( networkable( ), vmt_indexes::IClientEntity::GetClientClass )( networkable( ) );
	}

	// Renderable
	void getRenderBounds( Vector3& mins, Vector3& maxs ) {
		return Vmt::vfn<void( _tc* )( void*, Vector3&, Vector3& )>( renderable( ), vmt_indexes::IClientEntity::GetRenderBounds )( renderable( ), mins, maxs );
	}
	model_t* getModel( ) {
		return Vmt::vfn<model_t*( _tc* ) ( void* )>( renderable( ), vmt_indexes::IClientEntity::GetModel )( renderable( ) );
	}
	bool setupBones( Matrix3x4* mat, int maxBones, int boneFlags, float curTime = 0.f ) {
		return Vmt::vfn<bool( _tc* )( void*, Matrix3x4*, int, int, float )>( renderable( ), vmt_indexes::IClientEntity::SetupBones )( renderable( ), mat, maxBones, boneFlags, curTime );
	}

	// Helpers
	Weapon* getWep( ) {
		return Client::inst( ).entity<Weapon>( m_hActiveWeapon( ), true );
	}
	Vector3 eyePos( ) {
		return getAbsOrigin( ) + m_vecViewOffset( );
	}
	Vector3 getMid( ) {
		Vector3 mins{ }, maxs{ };
		getRenderBounds( mins, maxs );
		return getAbsOrigin( ) + Vector3( 0.f, 0.f, maxs[ 2 ] * .5f );
	}

	bool isDead( ) {
		return m_lifeState( ) == lifestate_t::dying || m_lifeState( ) == lifestate_t::dead;
	}

	inline std::string getModelName( ) {
		std::string retn{ "" };

		auto mdl{ getModel( ) };
		if ( mdl == nullptr )
			return retn;

		auto hdr = Engine::inst( ).GetStudioModel( mdl );
		if ( hdr == nullptr )
			return retn;

		if ( hdr->GetName( ) == nullptr )
			return retn;

		retn = std::string( hdr->GetName( ) );
		if ( retn.find( STR( ".mdl" ) ) != std::string::npos )
			retn.resize( retn.size( ) - 4 );

		auto pos = retn.find_last_of( "\\" );
		if ( pos != std::string::npos )
			retn.erase( 0, pos + 1 );

		pos = retn.find_last_of( "/" );
		if ( pos != std::string::npos )
			retn.erase( 0, pos + 1 );

		return retn;
	}

	inline bool isNpc( ){
		auto cl{ getClass( ) };
		if ( cl == nullptr )
			return false;

		if ( cl->m_nClassId == ClientClass::CAI_BaseNPC )
			return true;

#if _HL2 || _GMOD
		switch ( cl->m_nClassId ) {
		case ClientClass::CNPC_Barney:
		case ClientClass::CNPC_AntlionGuard:
		case ClientClass::CNPC_Barnacle:
		case ClientClass::CNPC_CombineGunship:
		case ClientClass::CNPC_Manhack:
		case ClientClass::CNPC_RollerMine:
		case ClientClass::CNPC_Vortigaunt:
			return true;

		default:
			return false;
		}
#endif
		return false;
	}

	inline bool isPlayer( ) {
		auto cl{ getClass( ) };
		if ( cl == nullptr )
			return false;
#if _GMOD
		if ( cl->m_nClassId == ClientClass::CGMOD_Player ||
			cl->m_nClassId == ClientClass::CHL2MP_Player )
			return true;
#endif

		return false;
	}

	inline bool isFriendlyNpc( ) {
		if ( !isNpc( ) )
			return true;

		auto mdlname{ getModelName( ) };
		if ( mdlname == "" )
			return true;

		const auto npos{ std::string::npos };

		if ( mdlname.find( STR( "Scanner" ) ) != npos )
			return true;
		if ( mdlname.find( STR( "camera" ) ) != npos )
			return true;

#if _HL2
		if ( mdlname.find( STR( "Police" ) ) != npos )
			return false;
		if ( mdlname.find( STR( "Combine" ) ) != npos )
			return false;
#endif
		if ( mdlname.find( STR( "Barnacle" ) ) != npos )
			return false;
		if ( mdlname.find( STR( "Antlion" ) ) != npos )
			return false;		
		if ( mdlname.find( STR( "manhack" ) ) != npos )
			return false;
		if ( mdlname.find( STR( "headcrab" ) ) != npos )
			return false;
		if ( mdlname.find( STR( "Classic" ) ) != npos )
			return false;
		if ( mdlname.find( STR( "Fast" ) ) != npos )
			return false;
		if ( mdlname.find( STR( "Poison" ) ) != npos )
			return false;
		if ( mdlname.find( STR( "gunship" ) ) != npos )
			return false;
		if ( mdlname.find( STR( "Floor_turret" ) ) != npos )
			return false;

		return true;
	}

#if _HL2 || _GMOD
	inline bool aimAtMidpoint( ) {
		auto mdlname{ getModelName( ) };
		const auto npos{ std::string::npos };

		if ( mdlname.find( STR( "Barnacle" ) ) != npos )
			return true;
		if ( mdlname.find( STR( "Antlion" ) ) != npos )
			return true;
		if ( mdlname.find( STR( "headcrab" ) ) != npos )
			return true;
		if ( mdlname.find( STR( "Floor_turret" ) ) != npos )
			return true;
		if ( mdlname.find( STR( "manhack" ) ) != npos )
			return true;

		return false;
	}
#endif

#if _HL2 || _TF2
	inline bool isVehicle( ) {
		auto cl{ getClass( ) };
		if ( cl == nullptr )
			return false;
#if _HL2
		if ( cl->m_nClassId == ClientClass::CPropJeep ||
			cl->m_nClassId == ClientClass::CPropAirboat ||
			cl->m_nClassId == ClientClass::CBaseHelicopter )
			return true;
#endif

		return false;
	}
#endif
	inline bool isPickup( ) {
		auto cl{ getClass( ) };
		if ( cl == nullptr )
			return false;

		std::string mdlName = getModelName( );

#if _HL2
		if ( mdlName.find( STR( "CombineThumper002" ) ) != std::string::npos )
			return false;
		if ( mdlName.find( STR( "Combine" ) ) != std::string::npos )
			return false;

		//if ( mdlName.find( STR( "props" ) ) )
		//	return false;

		return cl->m_nClassId == ClientClass::CBaseAnimating;
#endif
	}

	inline bool isWeapon( ) {
		auto cl{ getClass( ) };
		if ( cl == nullptr )
			return false;

		switch ( cl->m_nClassId ) {
#if _HL2 || _GMOD
		case ClientClass::CHLMachineGun:
		case ClientClass::CWeapon357:
		case ClientClass::CWeaponAlyxGun:
		case ClientClass::CWeaponAnnabelle:
		case ClientClass::CWeaponAR2:
		case ClientClass::CWeaponBugBait:
		case ClientClass::CWeaponCrossbow:
		case ClientClass::CWeaponCrowbar:
		case ClientClass::CWeaponFrag:
		case ClientClass::CWeaponGaussGun:
		case ClientClass::CWeaponPhysCannon:
		case ClientClass::CWeaponPistol:
		case ClientClass::CWeaponRPG:
		case ClientClass::CWeaponShotgun:
		case ClientClass::CWeaponSMG1:
		case ClientClass::CWeaponStunStick:
#endif
			return true;
		default:
			return false;
		}

		return false;
	}

	std::array<Vector3, 8> getPoints( ) {
		if ( !this )
			return{ };

		Vector3 mins{ },
			maxs{ },
			origin{ };
		origin = getAbsOrigin( );
		getRenderBounds( mins, maxs );

		std::array<Vector3, 8> screenPositions{ }, corners{
			Vector3( mins[ 0 ], mins[ 1 ], mins[ 2 ] ),
			Vector3( mins[ 0 ], mins[ 1 ], maxs[ 2 ] ),
			Vector3( mins[ 0 ], maxs[ 1 ], mins[ 2 ] ),
			Vector3( mins[ 0 ], maxs[ 1 ], maxs[ 2 ] ),
			Vector3( maxs[ 0 ], mins[ 1 ], mins[ 2 ] ),
			Vector3( maxs[ 0 ], mins[ 1 ], maxs[ 2 ] ),
			Vector3( maxs[ 0 ], maxs[ 1 ], mins[ 2 ] ),
			Vector3( maxs[ 0 ], maxs[ 1 ], maxs[ 2 ] )
		};

		Matrix3x4 mat = getAbsAngles( ).matrix( getAbsOrigin( ) );
		for ( int p{ }; p < 8; p++ ) {
			corners[ p ] = corners[ p ].transform( mat );
			if ( !Engine::inst( ).toScr( corners[ p ], screenPositions[ p ] ) )
				continue;
		}
		return screenPositions;
	}

	inline studiohdr_t* getHdr( ) {
		if ( !this )
			return nullptr;

		if ( getModel( ) == nullptr )
			return nullptr;

		return Engine::inst( ).GetStudioModel( getModel( ) );
	}

	inline mstudiohitboxset_t* getHitboxSet( ) {
		if ( !this )
			return nullptr;

		if ( getModel( ) == nullptr )
			return nullptr;

		auto hdr = Engine::inst( ).GetStudioModel( getModel( ) );
		if ( hdr == nullptr )
			return nullptr;

		auto nSet = m_nHitboxSet( );
		if ( nSet > 0x64 )
			return nullptr;

		return hdr->GetHitboxSet( nSet );
	}

	inline Matrix3x4* getBoneCache( ) {
		static Matrix3x4 mat[ 128 ]{ };

		if ( !this )
			return nullptr;

		if ( !setupBones( mat, 128, 0x100 ) )
			return nullptr;

		return reinterpret_cast<Matrix3x4*>( std::addressof( mat ) );
	}

	bool getHighestHitbox( Vector3& out ) {
		Vector3 retn{ };
		if ( !this )
			return false;

		auto* mat = getBoneCache( );
		if ( mat == nullptr )
			return false;

		auto* hdr = getHdr( );
		if ( hdr == nullptr )
			return false;

		auto* set = getHitboxSet( );
		if ( set == nullptr )
			return false;

		float low = -9999.f;
		bool ret = false;
		Vector3 box{ };

		for ( int it = 0; it < set->numhitboxes; it++ ) {
			auto* hitbox = hdr->GetHitbox( it, m_nHitboxSet( ) );
			
			if ( !hitbox )
				continue;
			
			box = ( hitbox->bbmin.transform( mat[ hitbox->bone ] ) + hitbox->bbmax.transform( mat[ hitbox->bone ] ) ) * .5f;

			if ( box[ 2 ] > low ) {
				retn = box;
				low = box[ 2 ];
				ret = true;
			}
		}
		out = retn;
		return ret;
	}

	bool createHitboxBox( int& x, int& y, int& w, int& h ) {
		if ( !this )
			return false;

		int minx{ 9999 }, miny{ 9999 }, maxx{ }, maxy{ };
		mstudiobbox_t* hitbox{ };
		Vector3 pos{ }, ang{ };
		
		auto* mat = getBoneCache( );
		if ( mat == nullptr )
			return false;

		auto* hdr = getHdr( );
		if ( hdr == nullptr )
			return false;

		auto* set = getHitboxSet( );
		if ( set == nullptr )
			return false;	
	
		Vector3* screenPos = new( std::nothrow ) Vector3[ set->numhitboxes ], box{ };

		for ( int it = 0; it < set->numhitboxes; it++ ) {
			hitbox = hdr->GetHitbox( it, m_nHitboxSet( ) );
			if ( !hitbox ) {
				delete[ ] screenPos;
				return false;
			}

			box = ( hitbox->bbmin.transform( mat[ hitbox->bone ] ) + hitbox->bbmax.transform( mat[ hitbox->bone ] ) ) * .5f;

			if ( !Engine::inst( ).toScr( box, screenPos[ it ] ) ) {
				delete[ ] screenPos;
				return false;
			}
		}

		for ( int it = 0; it < set->numhitboxes; it++ ) {
			minx = min( minx, screenPos[ it ][ 0 ] );
			miny = min( miny, screenPos[ it ][ 1 ] );
			maxx = max( maxx, screenPos[ it ][ 0 ] );
			maxy = max( maxy, screenPos[ it ][ 1 ] );
		}	

		w = ( maxx - minx ) * 1.8f;
		h = ( maxy - miny ) * 1.2f;

		auto xdif = ( ( w - ( maxx - minx ) ) * 0.5f );
		auto ydif = ( ( h - ( maxy - miny ) ) * 0.5f );

		x = minx - xdif;
		y = miny - ydif;

		delete[ ] screenPos;

		return true;
	} 

	bool createBoundsBox( int& x, int& y, int& w, int& h ) {
		if ( !this )
			return false;

		int tempx{ },
			tempy{ },
			tempw{ },
			temph{ };

		Vector3 mins{ },
			maxs{ },
			origin{ };

		origin = getAbsOrigin( );
		getRenderBounds( mins, maxs );

		Vector3 screenPositions[ 8 ]{ }, corners[ 8 ]{
			Vector3( mins[ 0 ], mins[ 1 ], mins[ 2 ] ),
			Vector3( mins[ 0 ], mins[ 1 ], maxs[ 2 ] ),
			Vector3( mins[ 0 ], maxs[ 1 ], mins[ 2 ] ),
			Vector3( mins[ 0 ], maxs[ 1 ], maxs[ 2 ] ),
			Vector3( maxs[ 0 ], mins[ 1 ], mins[ 2 ] ),
			Vector3( maxs[ 0 ], mins[ 1 ], maxs[ 2 ] ),
			Vector3( maxs[ 0 ], maxs[ 1 ], mins[ 2 ] ),
			Vector3( maxs[ 0 ], maxs[ 1 ], maxs[ 2 ] )
		};

		Engine::inst( ).Dimensions( tempw, temph );
		tempx = tempw * 2;
		tempy = temph * 2;
		tempw = temph = -9999;

		Matrix3x4 mat = getAbsAngles( ).matrix( getAbsOrigin( ) );
		for ( int p{ }; p < 8; p++ ) {
			corners[ p ] = corners[ p ].transform( mat );
			if ( !Engine::inst( ).toScr( corners[ p ], screenPositions[ p ] ) )
				continue;

			tempx = min( tempx, screenPositions[ p ][ 0 ] );
			tempy = min( tempy, screenPositions[ p ][ 1 ] );
			tempw = max( tempw, screenPositions[ p ][ 0 ] );
			temph = max( temph, screenPositions[ p ][ 1 ] );
		}

		tempw -= tempx;
		temph -= tempy;

		x = tempx;
		y = tempy;
		w = tempw;
		h = temph;

		return true;
	}

};