#pragma once

#include "../engine/se_engine.h"

using namespace vmt_indexes;

class Entity;
struct ClientClass_t;
class Client {
private:
	Interface VClient{ },
		VClientEntityList{ },
		GameMovement{ },
		IEffects{ };

	Address IClientMode{ },
		IInput{ },
		CClientState{ };

	Globals* m_pGlobals{ };

	MD5_PseudoRandom m_pr{ };

	RandomSeed m_rseed{ };
	RandomFloat m_rfloat{ };
public:

	inline Client( ) :
		VClient{ Interfaces::inst( )[ STR( "VClient" ) ] },
		VClientEntityList{ Interfaces::inst( )[ STR( "VClientEntityList" ) ] },
		GameMovement{ Interfaces::inst( )[ STR( "GameMovement" ) ] },
		IEffects{ Interfaces::inst( )[ STR( "IEffects" ) ] } {

		auto& vstdlib = Peb::inst( )->get( STR( "vstdlib.dll" ) );

		m_rseed = vstdlib.exp( STR( "RandomSeed" ) ).to<RandomSeed>( );
		m_rfloat = vstdlib.exp( STR( "RandomFloat" ) ).to<RandomFloat>( );

		auto vmt = std::get<Address>( VClient ).get<uintptr_t*>( 1 );

#if _INSURGENCY
		m_pGlobals = Address( vmt[ 11 ] + 0x5 ).get<Globals*>( 2 );
		IClientMode = Address( Address( vmt[ 10 ] ).disp( 0x4 ) + 0x1 ).get( 2 );
		IInput = Address( vmt[ 15 ] + 0x2 ).get( 1 );
#endif

#if _CSGO
		IInput = Address( vmt[ 15 ] + 0x1 ).get( 1 );		
		IClientMode = Address( vmt[ 10 ] + 0x5 ).get( 2 );
		CClientState = Address( Pattern( STR( "engine.dll" ), STR( "8B 3D ?? ?? ?? ?? 8A F9" ) ) + 0x2 ).get( 2 );
#elif _GMOD || _ORANGEBOX
		IInput = Address( vmt[ 15 ] + 0x2 ).get( 2 );	
		IClientMode = Pattern( STR( "client.dll" ), STR( "E8 ?? ?? ?? ?? A3 ?? ?? ?? ?? E8 ?? ?? ?? ??" ), true ).at( 0x6 ).get( 1 );
#endif

#if _GMOD
		m_pGlobals = Address( Pattern( vmt[ 0 ], 0x200, STR( "89 0D ?? ?? ?? ?? E8 ?? ?? ?? ?? 8D 45 08" ) ) + 0x2 ).get<Globals*>( 2 );
#elif _ORANGEBOX || _CSGO
		m_pGlobals = Address( Pattern( vmt[ 0 ], 0x200, STR( "A3 ?? ?? ?? ??" ) ) + 0x1 ).get<Globals*>( 2 );
#endif
#if _GMOD
		size_t which = 2;
#else
		size_t which{ };
#endif

		auto ctx_buf0 = Pattern( STR( "client.dll" ), STR( "01 23 45 67" ), false, which );
		if ( ctx_buf0 ) {
			m_pr = Pattern( ctx_buf0 - 0x40, 0x80, STR( "55 8B EC" ) ).to<MD5_PseudoRandom>( );
		}


#if _SE_DEBUG
		io::w_printf( STR( "[Client] Globals ... (0x%X)\n" ), m_pGlobals );
		io::w_printf( STR( "[Client] IClientMode ... (0x%X)\n" ), IClientMode );
		io::w_printf( STR( "[Client] IInput ... (0x%X)\n" ), IInput );
		io::w_printf( STR( "[Client] MD5_PseudoRandom ... (0x%X)\n" ), m_pr ); //

		//auto pr = Pattern( STR( "client.dll" ), STR( "55 8B EC 83 EC 68 6A 58" ) );
		//io::w_printf( STR( "[Client] MD5_PseudoRandomCorrect ... (0x%X)\n" ), pr );

		io::w_printf( STR( "[Vstd] RandomSeed ... (0x%X)\n" ), m_rseed );
		io::w_printf( STR( "[Vstd] RandomFloat ... (0x%X)\n" ), m_rfloat );
#if _CSGO
		io::w_printf( STR( "[Client] CClientState ... (0x%X)\n" ), CClientState );
#endif
#endif // _SE_DEBUG
	}

	// VClient
	auto* getClientClass( ) {
		static Vmt vmt( std::get<Address>( VClient ) );
		return vmt.get<ClientClass_t*( _tc* ) ( void* )>( VClient::GetClientClass )( std::get<Address>( VClient ) );
	}

	// VClientEntityList
	template<typename t = Entity>t* entity( uint32_t idx, bool handle = false ) {
		static Vmt vmt( std::get<Address>( VClientEntityList ) );
		return vmt.get<t*( _tc* ) ( void*, uint32_t )>( handle ? VClientEntityList::GetEntityByHandle : VClientEntityList::GetEntityByHandle - 1 )( std::get<Address>( VClientEntityList ), idx );
	}

	int32_t highestEntIndex( ) {
		static Vmt vmt( std::get<Address>( VClientEntityList ) );
		return vmt.get<int32_t( _tc* )( void* )>( VClientEntityList::GetHighestEntityIndex )( std::get<Address>( VClientEntityList ) );
	}

	auto md5_pr( uint32_t seed ) {
		return m_pr( seed );
	}
	
	auto r_seed( uint32_t seed ) {
		return m_rseed( seed );
	}
	auto r_float( float min, float max ) {
		return m_rfloat( min, max );
	}


	// Helpers
	auto& globals( ) {
		return *m_pGlobals;
	}

	auto& getClientmode( ) {
		return IClientMode;
	}
	auto& getInput( ) {
		return IInput;
	}
	auto& getClient( ) {
		return std::get<Address>( VClient );
	}

#if _CSGO
	auto& getClientstate( ) {
		return CClientState;
	}
#endif

#if defined(_INSURGENCY) || defined(_CSGO)
	auto* getCmd( int32_t sequenceNum ) {
		static Vmt vmt( IInput );
		return vmt.get<Cmd*( _tc* ) ( void*, int32_t, int32_t )>( vmt_indexes::IInput::GetUsercmd )( IInput, -1, sequenceNum );
	}
#else
	auto* getCmd( int32_t sequenceNum ) {
		static Vmt vmt( IInput );
		return vmt.get<Cmd*( _tc* ) ( void*, int32_t )>( vmt_indexes::IInput::GetUsercmd )( IInput, sequenceNum );
	}
#endif

	static Entity* localPly( ) {
		if ( !Engine::inst( ).Connected( ) || !Engine::inst( ).InGame( ) )
			return nullptr;

		return inst( ).entity( Engine::inst( ).Local( ) );
	}

	// Singleton
	static Client& inst( ) {
		static Client* singleton{ };
		if ( singleton == nullptr )
			singleton = new Client( );

		return *singleton;
	}
};