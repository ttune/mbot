#include "ok.h"

namespace Patches {
	Vmt client{ },
		input{ };

	uint8_t __fastcall WriteUsercmdDeltaToBuffer( void*, void*, void* buf, int from, int to, bool isNew ) {
		using writeUserCmdFn = void(*)( void*, Cmd*, Cmd* );
		static writeUserCmdFn WriteUsercmd{ };
		if ( !WriteUsercmd ) {
#if _CSGO
			WriteUsercmd = Pattern( STR( "client.dll" ), STR( "55 8B EC 83 E4 F8 51 53 56 8B D9 8B 0D" ) ).to<writeUserCmdFn>( );
#elif _INSURGENCY
			WriteUsercmd = Pattern( STR( "client.dll" ), STR( "55 8B EC 8B 0D ?? ?? ?? ?? 81 F9 ?? ?? ?? ?? 75 07 A1 ?? ?? ?? ?? EB 05 8B 01 FF 50 34 53" ) ).to<writeUserCmdFn>( );
#endif

#if _SE_DEBUG
			io::w_printf( STR( "[Input] WriteUsercmd ... 0x%X\n" ), WriteUsercmd );
#endif
		}

		Cmd *cmdFrom{ },
			*cmdTo{ },
			nullCmd{ };

		if ( from == -1 )
			cmdFrom = &nullCmd;
		else {
			cmdFrom = Client::inst( ).getCmd( from );
			if ( !cmdFrom )
				cmdFrom = &nullCmd;
		}
		cmdTo = Client::inst( ).getCmd( to );
		if ( !cmdTo )
			cmdTo = &nullCmd;

		WriteUsercmd( buf, cmdTo, cmdFrom );

		return Address( buf ).at<uint8_t>( 0x10 );
	}

	void __fastcall FrameStageNotify( void* ecx, void*, uint32_t stage ) {
		static auto& noeffects = Vars::inst( ).misc.novisrecoil;
		static Address pa{ };
		static Vector3 old{ }, *punch{ };

		if ( !pa )
			pa = Netvars::netvar( DT_BasePlayer, 0x54BB6894 /* m_vecPunchAngle */ );

		auto orig = client.get<void( _tc* )( void*, uint32_t )>( vmt_indexes::VClient::FrameStageNotify );

		if ( stage == 5 ) {
			Entity* loc{ Client::localPly( ) };
			if ( loc != nullptr ) {
				if ( loc->m_lifeState( ) == lifestate_t::alive ) {
					punch = Address( Address( loc ) + pa ).to<Vector3*>( );			
					if ( Address::safe( punch ) ) {
						old = *punch;
						*punch = Vector3{ };
					}
				}
				else punch = nullptr;
			}
		}
		orig( ecx, stage );

		if ( Address::safe( punch ) )
			*punch = old;
	}

	void __fastcall CreateMove( void* ecx, void*, int seq, float frameTime, bool active ) {
		client.get<void( _tc* )( void*, int, float, bool )>( vmt_indexes::VClient::CreateMove )( ecx, seq, frameTime, active );
	}
}

namespace Frames {
	Vmt clientMode{ },
		panel{ },
		trace{ },
		prediction{ };

	bool __fastcall CreateMove( void* ecx, void*, float sampleTime, Cmd* cmd ) {
		static auto& silentaim = Vars::inst( ).aim.silent;
		
		if ( !cmd->m_cmdNum )
			return true;
		if ( Client::localPly( ) == nullptr )
			return true;

#if _HL2 || _L4D2
		if ( Input::get( 'F' ) ) {  // speedhack
			static float factor{ };
			static auto& speedhank = Vars::inst( ).misc.speedhank;

			Stack stack;
			Address retnAddr = stack.next( ).next( ).next( ).to<uintptr_t>( ) + 0x4;

			if ( factor ) {
				retnAddr.set<Address>( retnAddr.get( 1 ) - 0x5 );
				cmd->m_Impulse = 0;
				factor -= 1.f;
			}
			else
				factor = speedhank.get( );
		}
#endif

		Aimbot::inst( ).run( cmd );
		Triggerbot::inst( ).run( cmd );

		if ( Input::get( VK_F5 ) )
			io::w_printf( STR( "[mbot] Local %X\n" ), Client::localPly( ) );

		return !silentaim;
	}

	void __fastcall PaintTraverse( void* ecx, void*, Vgui::VPanel vpanel, bool forceRepaint, bool allowForce ) {
		panel.get<void( _tc* )( void*, Vgui::VPanel, bool, bool )>( vmt_indexes::VGUI_Panel::PaintTraverse )( ecx, vpanel, forceRepaint, allowForce );

		if ( Vgui::inst( ).canDraw( vpanel ) ) {
			Render::inst( ).Text( 10, 10, colors::white, Render::tahoma12, Render::left, "%s mbot", Module::getProcNameTruncated( ).c_str( ) );
			
			Console::inst( ).run( );
			Esp::inst( ).run( );
		}
	}

	LONG_PTR __stdcall wndProc( HWND wnd, uint32_t msg, WPARAM wParam, LPARAM lParam ) {
		Input::inst( ).run( msg, wParam, lParam );

		return CallWindowProc( Input::inst( ).orig( ), wnd, msg, wParam, lParam );
	}

#if _GMOD
	int32_t __fastcall GetPointContents( void* ecx, void*, const Vector3& pos, Entity** ent ) {
		static Address ret{ };
		Stack stack;

		if ( !ret ) {
			ret = Pattern( STR( "client.dll" ), STR( "?? ?? ?? ?? ?? ?? ?? ?? ??" ) ); // antipaste - sorry
#if _SE_DEBUG
			io::w_printf( STR( "[Spread] gay lua func ret ... 0x%X\n" ), ret );
#endif
		}	

		if ( _ReturnAddress( ) == ret ) {
			Weapon::spread( ) = stack.next( ).at( -0x0 ).as<Vector3>( );
		}

		return trace.get<int32_t( _tc* )( void*, const Vector3&, Entity** )>( vmt_indexes::EngineTraceClient::GetPointContents )( ecx, pos, ent );
	}
#endif

	bool __fastcall InPrediction( void* ecx, void* ) {
		static Address cpv{ };
		static auto& noeffects = Vars::inst( ).misc.novisrecoil;
	
		Stack stack;
		auto inPred = prediction.get<bool( _tc* )( void* )>( vmt_indexes::GameMovement::InPrediction )( ecx );
	
		if ( !cpv ) {
#if _HL2
			cpv = Pattern( STR( "client.dll" ), STR( "8B 40 38 FF D0 84 C0 75 08" ) );
#elif _GMOD
			cpv = Pattern( STR( "client.dll" ), STR( "84 C0 75 08 53" ) );
#elif _CSGO
			cpv = Pattern( STR( "client.dll" ), STR( "84 C0 75 08 57 8B CE E8 ?? ?? ?? ?? 8B 06" ) ); 
#endif
			if ( !cpv )
				return inPred;
#if _HL2
			cpv += 0x5;
#endif

#if _SE_DEBUG
			io::w_printf( STR( "[Pred] CalcPlayerView ... 0x%X\n" ), cpv );
#endif
		}

		if ( cpv == _ReturnAddress( ) && noeffects ) {
			auto* loc = Client::localPly( );
			if ( loc == nullptr )
				return inPred;

			auto* va = stack.next( ).at( 0xC ).get<Vector3*>( 1 );
			( *va ) -= loc->m_vecPunchAngle( );

			return true;
		}

		return inPred;
	}


	inline void unhook( ) {
		clientMode.unhook( );
		panel.unhook( );
	}
}

unsigned long __stdcall Ok( void* lparam ) {
#if _CSGO || _GMOD
	while ( !GetModuleHandleA( STR( "serverbrowser.dll" ) ) )
		sleep_ms( 0x64 );
#endif

	Console::inst( ).setTitle( STR( "mbot") );

	Netvars::dump( STR( "netvars.log" ) );
	Interfaces::dump( STR( "interfaces.log" ) );
	Input::inst( FindWindowA( STR( "Valve001" ), nullptr ) ).attach( Frames::wndProc );

	Frames::clientMode = Vmt( Client::inst( ).getClientmode( ), true );
	Frames::clientMode.swap( vmt_indexes::IClientMode::CreateMove, Frames::CreateMove );

	Frames::panel = Vmt( Vgui::inst( ).getPanel( ), true );
	Frames::panel.swap( vmt_indexes::VGUI_Panel::PaintTraverse, Frames::PaintTraverse );

	//Patches::client = Vmt( Client::inst( ).getClient( ), true );
	//Patches::client.swap( vmt_indexes::VClient::FrameStageNotify, Patches::FrameStageNotify );

	Frames::trace = Vmt( Trace::inst( ).traceClient( ), true );
#if _GMOD
	Frames::trace.swap( vmt_indexes::EngineTraceClient::GetPointContents, Frames::GetPointContents );
#endif

	Frames::prediction = Vmt( Prediction::inst( ).pred( ), true );
#if _CSGO || _GMOD || _HL2
	Frames::prediction.swap( vmt_indexes::GameMovement::InPrediction, Frames::InPrediction );
#endif

	getchar( );
	Frames::unhook( );

	return 0;
}

int __stdcall DllMain( HMODULE inst, int reason, void* ) {
	if ( reason == 1 ) {
		DisableThreadLibraryCalls( inst );
		CreateThread( 0, 0, Ok, inst, 0, 0 );
	} 
	return 1;
}
