#pragma once

#include <m_maddie.h>

using namespace std;

class Convar {
public:
	using callback = void(*)( Convar* );
private:
	float m_data{ };
	string m_name{ }, m_desc{ };

	callback m_callback = 0;
public:
	Convar( ) { }
	Convar( const string& name, float val = 0.f, const string& desc = "", callback fn = nullptr ) {
		m_name = name;
		m_data = val;
		m_desc = desc;
		m_callback = fn;
	}

	inline string& name( ) { return m_name; }
	inline float& get( ) { return m_data; }

	inline float* ptr( ) { return &m_data; }

	template<typename t = uint32_t>t to( ) {
		return ( t ) m_data;
	}

	operator bool() const {
		return m_data > 0.f;
	}
};

class Convars {
private:
	vector<Convar> m_convars{ };
public:
	Convars( ) {}

	inline void add( const string& name, float val = 0.f, const string& desc = "", Convar::callback fn = nullptr ) {
		m_convars.push_back( Convar( name, val, desc, fn ) );
	}

	void operator += ( Convar cv ) {
		m_convars.push_back( cv );
	}

	inline auto& get( const std::string& name ) {
		for ( auto& cv : m_convars ) {
			if ( cv.name( ) == name )
				return cv;
		}
		return m_convars.front( );
	}

	static Convars& inst( ) {
		static Convars* singleton{ };
		if ( singleton == nullptr )
			singleton = new Convars;

		return *singleton;
	}
};