#pragma once

#include "..\main\inc.h"
#include "vars.h"

class Esp {
private:
	Entity* m_local{ };
public:
	inline Esp( ) { }

	void drawBoxes( Entity * ent, Color col );

	void run( );

	static Esp& inst( ) {
		static Esp* singleton{ };
		if ( singleton == nullptr )
			singleton = new Esp( );

		return *singleton;
	}
};