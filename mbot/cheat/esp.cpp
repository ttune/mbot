#include "esp.h"

void Esp::drawBoxes( Entity* ent, Color col ) {
	mstudiobbox_t* hitbox{ };
	Vector3 pos{ }, ang{ };
	static Matrix3x4 mat[ 128 ]{ };

	if ( !ent->setupBones( mat, 128, 0x100 ) )
		return;
	if ( ent->getModel( ) == nullptr )
		return;

	auto hdr = Engine::inst( ).GetStudioModel( ent->getModel( ) );
	if ( hdr == nullptr )
		return;

	auto nSet = ent->m_nHitboxSet( );
	if ( nSet > 100 )
		return;

	auto set = hdr->GetHitboxSet( nSet );
	if ( set == nullptr )
		return;

	using Matrix34 = float[ 3 ][ 4 ];
	auto matrixFromAngles = [ ] ( Vector3 angles, Vector3 origin, Matrix34& out ) {

		Vector3 vfwd{ },
			vright{ },
			vup{ };

		vfwd = angles.fwd( );
		vright = angles.right( );
		vup = angles.up( );

		out[ 0 ][ 0 ] = vfwd[ 0 ];
		out[ 1 ][ 0 ] = vfwd[ 1 ];
		out[ 2 ][ 0 ] = vfwd[ 2 ];
		out[ 0 ][ 1 ] = vright[ 0 ];
		out[ 1 ][ 1 ] = vright[ 1 ];
		out[ 2 ][ 1 ] = vright[ 2 ];
		out[ 0 ][ 2 ] = vup[ 0 ];
		out[ 1 ][ 2 ] = vup[ 1 ];
		out[ 2 ][ 2 ] = vup[ 2 ];
		out[ 0 ][ 3 ] = origin[ 0 ];
		out[ 1 ][ 3 ] = origin[ 1 ];
		out[ 2 ][ 3 ] = origin[ 2 ];
	
	};

	auto vectorTransform = [ ] ( Vector3 in, Matrix34& mat ) {
		Vector3 out{ };

		Vector3 arr[ 3 ]{
			Vector3( mat[ 0 ][ 0 ], mat[ 0 ][ 1 ], mat[ 0 ][ 2 ] ),
			Vector3( mat[ 1 ][ 0 ], mat[ 1 ][ 1 ], mat[ 1 ][ 2 ] ),
			Vector3( mat[ 2 ][ 0 ], mat[ 2 ][ 1 ], mat[ 2 ][ 2 ] )
		};

		out[ 0 ] = in.dot_product( arr[ 0 ] ) + mat[ 0 ][ 3 ]; // bf1 matrix
		out[ 1 ] = in.dot_product( arr[ 1 ] ) + mat[ 1 ][ 3 ];
		out[ 2 ] = in.dot_product( arr[ 2 ] ) + mat[ 2 ][ 3 ];

		return out;
	};

	for ( int it = 0; it < set->numhitboxes; it++ ) {
		hitbox = hdr->GetHitbox( it, ent->m_nHitboxSet( ) );
		if ( !hitbox )
			continue;

		Vector3 mins = hitbox->bbmin,
			maxs = hitbox->bbmax;

		std::array<Vector3, 8> screenPositions, corners = {
			Vector3( mins[ 0 ], mins[ 1 ], mins[ 2 ] ),
			Vector3( mins[ 0 ], mins[ 1 ], maxs[ 2 ] ),
			Vector3( mins[ 0 ], maxs[ 1 ], mins[ 2 ] ),
			Vector3( mins[ 0 ], maxs[ 1 ], maxs[ 2 ] ),
			Vector3( maxs[ 0 ], mins[ 1 ], mins[ 2 ] ),
			Vector3( maxs[ 0 ], mins[ 1 ], maxs[ 2 ] ),
			Vector3( maxs[ 0 ], maxs[ 1 ], mins[ 2 ] ),
			Vector3( maxs[ 0 ], maxs[ 1 ], maxs[ 2 ] )
		};

		for ( int p{ }; p < 8; p++ ) {
			corners[ p ] = vectorTransform( corners[ p ], *( Matrix34* ) &mat[ hitbox->bone ] );
			if ( !Engine::inst( ).toScr( corners[ p ], screenPositions[ p ] ) )
				return;
		}

		col[ 3 ] = 150;
		auto drawLinePath = [ & ] ( std::array<Vector3, 8>& points, int a, int b ) -> void {
			Render::inst( ).Line( points[ a ][ 0 ], points[ a ][ 1 ], points[ b ][ 0 ], points[ b ][ 1 ], col );
		};

		drawLinePath( screenPositions, 0, 1 );
		drawLinePath( screenPositions, 0, 4 );
		drawLinePath( screenPositions, 5, 1 );
		drawLinePath( screenPositions, 5, 4 );

		drawLinePath( screenPositions, 2, 3 );
		drawLinePath( screenPositions, 2, 6 );
		drawLinePath( screenPositions, 7, 3 );
		drawLinePath( screenPositions, 7, 6 );

		drawLinePath( screenPositions, 0, 2 );
		drawLinePath( screenPositions, 4, 6 );
		drawLinePath( screenPositions, 5, 7 );
		drawLinePath( screenPositions, 1, 3 );
	}
}

void Esp::run( ) {
	int x, y, w, h{ };
	Color draw{ };

	m_local = Client::localPly( );
	if ( !m_local )
		return;

	if ( !Vars::inst( ).vis.esp.active || Vars::inst( ).vis.active.get( ) )

	for ( auto it = Client::inst( ).highestEntIndex( ); it > 1; it-- ) {
		auto* ent = Client::inst( ).entity( it );
		if ( !ent || ent == m_local )
			continue;

		auto* cl = ent->getClass( );
		if ( !cl )
			continue;

		bool player = it < 64;

		if ( ent->createHitboxBox( x, y, w, h ) ) {
#if _HL2 || _GMOD
			if ( ent->isNpc( ) || ent->isPlayer( ) ) {
				if ( !ent->isDead( ) ) {
					bool visible = Trace::inst( ).isVisible( m_local->eyePos( ), ent->getMid( ), ent );

					if ( ent->isPlayer( ) ) {
						draw = ent->m_iTeamNum( ) == m_local->m_iTeamNum( ) ?
							( visible ? colors::Teams::allieVisible : colors::Teams::allieInvisible ) :
							( visible ? colors::Teams::axisVisible : colors::Teams::axisInvisible );
					}
					else if ( ent->isNpc( ) ) {
						draw = ent->isFriendlyNpc( ) ?
							( visible ? colors::wisteria : colors::lightWisteria ) :
							( visible ? colors::Teams::axisVisible : colors::Teams::axisInvisible );
					}

#if _GMOD
					if ( cl->m_nClassId != ClientClass::CGMOD_Player )
						draw = visible ? colors::green : colors::lightGreen;
#endif

					Render::inst( ).Box( x, y, w, h, draw );
					Render::inst( ).Text( x + w / 2, y - 15, colors::white, IRender::tahoma12, IRender::Align::center, "%s", ent->getModelName( ).c_str( ) );
					//Render::inst( ).Text( x + w + 4, y, colors::white, IRender::tahoma12, IRender::Align::left, "X: 0x%X", ent );

					if ( ent->getWep( ) ) {
						if ( ent->getWep( )->isWeapon( ) ) {
							Render::inst( ).Text( x + w / 2, y + h + 3, colors::white, IRender::tahoma12, IRender::Align::center, "%s", ent->getWep( )->getName( ).c_str( ) );
						}
					}

					drawBoxes( ent, draw );
				}
			}
			else if ( ent->isWeapon( ) ) {
				if ( ( ( Weapon* ) ent )->getOwner( ) == nullptr ) {
					if ( ent->createBoundsBox( x, y, w, h ) ) {
						Render::inst( ).Box( x, y, w, h, colors::white );
						Render::inst( ).Text( x + w / 2, y - 15, colors::white, IRender::tahoma12, IRender::Align::center, "%s", ( ( Weapon* ) ent )->getName( ).c_str( ) );
					}
				}
			}
#if _HL2 || _TF2
			else if ( ent->isPickup( ) ) {
				if ( ent->createBoundsBox( x, y, w, h ) ) {
					Render::inst( ).Box( x, y, w, h, colors::white );
					Render::inst( ).Text( x + w / 2, y - 15, colors::white, IRender::tahoma12, IRender::Align::center, "%s", ent->getModelName( ).c_str( ) );
				}
			}
			else if ( ent->isVehicle( ) ) {
				drawBoxes( ent, colors::white );
			}
#endif
		}
		
#endif
	}
}
