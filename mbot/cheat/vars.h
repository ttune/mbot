#pragma once

#include "../main/m_convars.h"

struct Vars {
	struct _aim {
		struct _legit {
			Convar smooth,
				aimtime,
				bezier,
				noise;
		};
		Convar active,
			teamfire,
			silent,
			psilent,
			aimkey,
			aimtype,
			hitbox,
			autofire,
			npcs,
			fov;
		_legit legit;
	};

	struct _vis {
		struct _esp {
			Convar active,
				npcs,
				boxes,
				health,
				hitboxes,
				weapon,
				world,
				name;
		};
		struct _chams {
			Convar active,
				flat;
		};
		Convar active;

		_esp esp;
		_chams chams;
	};

	struct _misc {
		Convar active,
			bunnyhop,
			rapidfire,
			nospread,
			norecoil,
			novisrecoil,
			debugtrace,
			speedhank;
	};

	void init( ) {
		
		// aim
		aim.active = Convar( STR( "Active" ), 0.f );
		aim.aimkey = Convar( STR( "Aimkey" ) );
		aim.silent = Convar( STR( "Silent" ), 1.f );
		aim.teamfire = Convar( STR( "Teamfire" ) );
		aim.aimtype = Convar( STR( "Aimtype" ) );
		aim.hitbox = Convar( STR( "Hitbox" ) );
		aim.fov = Convar( STR( "Field of view" ) );
		aim.psilent = Convar( STR( "Choke" ) );
		aim.npcs = Convar( STR( "Aim at NPCs" ), 1.f );
		aim.autofire = Convar( STR( "Autofire" ), 1.f );

		aim.legit.aimtime = Convar( STR( "Aimtime" ), 1.f );
		aim.legit.bezier = Convar( STR( "Bezier" ) );
		aim.legit.noise = Convar( STR( "Noise" ) );
		aim.legit.smooth = Convar( STR( "Smooth" ) );

		// vis
		vis.active = Convar( STR( "Active" ) );
		
		vis.chams.active = Convar( STR( "Active" ) );
		vis.chams.flat = Convar( STR( "Flat" ) );

		vis.esp.active = Convar( STR( "Active" ) );
		vis.esp.boxes = Convar( STR( "Boxes" ) );
		vis.esp.health = Convar( STR( "Health" ) );
		vis.esp.hitboxes = Convar( STR( "Hitboxes" ) );
		vis.esp.weapon = Convar( STR( "Weapon" ) );
		vis.esp.name = Convar( STR( "Name" ) );
		vis.esp.world = Convar( STR( "World" ) );
		vis.esp.npcs = Convar( STR( "Show NPCs" ), 1.f );


		// misc
		misc.active = Convar( STR( "Active" ), 1.f );
		misc.bunnyhop = Convar( STR( "Bunnyhop" ), 1.f );
		misc.rapidfire = Convar( STR( "Rapidfire" ), 1.f );
		misc.norecoil = Convar( STR( "No Recoil" ), 1.f );
		misc.nospread = Convar( STR( "No Spread" ), 1.f );
		misc.novisrecoil = Convar( STR( "No Effects" ), 1.f );
		misc.speedhank = Convar( STR( "Speedhank" ), 6.f );
		misc.debugtrace = Convar( STR( "Debug info" ) );
	}

	_aim aim;
	_vis vis;
	_misc misc;

	static Vars& inst( ) {
		static Vars* singleton{ };
		if ( singleton == nullptr ) {
			singleton = new Vars;
			singleton->init( );
		}

		return *singleton;
	}
};