#include "aimbot.h"

inline bool::Aimbot::targetable( int index ) {
	static auto& teamfire = Vars::inst( ).aim.teamfire,
		aimtype = Vars::inst( ).aim.aimtype,
		npcs = Vars::inst( ).aim.npcs,
		hitbox = Vars::inst( ).aim.hitbox;

	auto* ent = Client::inst( ).entity( index );

	if ( ent == nullptr || ent == m_local )
		return false;
	if ( ent->m_lifeState( ) == lifestate_t::dead || ent->m_lifeState( ) == lifestate_t::dying )
		return false;
#if _HL2
	if ( ent->isFriendlyNpc( ) )
		return false;
#elif _GMOD
	if ( !ent->isPlayer( ) && ( npcs && ent->isFriendlyNpc( ) ) )
		return false;
#elif _CSGO || _CSS || _TF2
	if ( ent->m_iTeamNum( ) == m_local->m_iTeamNum( ) && !teamfire )
		return false;
#endif
	if ( aimtype.get( ) == 0 ) { //
		if ( !ent->aimAtMidpoint( ) ) {
			if ( !ent->getHighestHitbox( m_end ) ) {
				m_end = ent->getMid( );
			}
		}
		else
			m_end = ent->getMid( );
	}
	else if ( aimtype.get( ) == 1 ) {

	}

	if ( Trace::inst( ).isVisible( m_local->eyePos( ), m_end, ent ) ) {
		m_targetEnt = ent;
		return true;
	}

	return false;
}

inline void Aimbot::drop( )
{
	static auto& aimkey = Vars::inst( ).aim.aimkey;

	if ( aimkey ) {
		if ( !targetable( m_target ) || !Input::get( aimkey.to( ) ) ) {
			m_target = -1;
		}
	}
	else if ( !targetable( m_target ) ) {
		m_target = -1;
	}
}

bool Aimbot::autofire( )
{
	static auto& af = Vars::inst( ).aim.autofire;

	if ( m_wep->m_iClip1( ) <= 0 )
		return false;

	if ( af )
		m_cmd->m_Buttons |= buttons::attack;

	return true;
}

inline void Aimbot::aim( )
{
	static auto& aimkey = Vars::inst( ).aim.aimkey;

	static auto& smooth = Vars::inst( ).aim.legit.smooth;
	static auto& aimtime = Vars::inst( ).aim.legit.aimtime;

	if ( aimkey ) {
		if ( !Input::get( aimkey.to( ) ) || m_target == -1 ) {
			target( );
		}
	}
	else if ( m_target == -1 ) {
		target( );
	}

	drop( );

	if ( m_target != -1 ) {

		if ( !autofire( ) )
			return;

		m_aimtime += Client::inst( ).globals( ).m_flIntervalPerTick;

		if ( targetSwitched( ) )
			m_aimtime = 0.f;

		if ( m_aimtime > aimtime.get( ) )
			m_aimtime = aimtime.get( );

		m_cmd->m_viewAngles = ( m_end - m_local->eyePos( ) ).angle( );
		m_cmd->m_viewAngles[ 2 ] = 0.f;
	}
}

void Aimbot::iterativeSpreadFix( )
{

}

void Aimbot::noSpread( )
{
	auto getWavRoll = [ ] ( Vector3 dir ) {
		Vector3 forward( 1, -dir.v.x, dir.v.y );
		Vector3 up( 0, -dir.v.x, abs( dir.v.y + ( 1 / dir.v.y ) + ( 1 / dir.v.y ) ) );

		if ( dir.v.x > 0 && dir.v.y < 0 )
			up.v.y = abs( up.v.y );
		else if ( dir.v.x < 0 && dir.v.y < 0 )
			up.v.y = -abs( up.v.y );

		Vector3 left = up.cross_product( forward );

		return Math::toDeg( atan2f( left.v.z, ( left.v.y * forward.v.x ) - ( left.v.x * forward.v.y ) ) );
	};
	Vector3 fwd{ }, right{ }, up{ }, dir{ };

	auto spread = m_wep->getSpread( m_cmd->m_randomSeed );
	if ( spread.empty( ) )
		return;

	fwd = m_cmd->m_viewAngles.fwd( );
	right = m_cmd->m_viewAngles.right( );
	up = m_cmd->m_viewAngles.up( );

	dir = fwd + ( right * -spread[ 0 ] ) + ( up * -spread[ 1 ] );
	dir.normalize( );

#if _CSS || _CSGO
	// polak roll
	m_cmd->m_viewAngles -= Vector3( -Math::toDeg( std::atan( spread.length2d( ) ) ), 0, Math::toDeg( std::atan2( spread.v.x, spread.v.y ) ) );
#elif _GMOD
	// roll
	m_cmd->m_viewAngles = dir.angle( &m_cmd->m_viewAngles.up( ) );
	m_cmd->m_viewAngles[ 2 ] += getWavRoll( spread );
#else
	// p/y comp
	m_cmd->m_viewAngles = dir.angle( );
#endif
	
}

void Aimbot::misc( )
{
	if ( Vars::inst( ).misc.bunnyhop ) { // bhop

		if ( m_cmd->m_Buttons & buttons::jump ) {
			if ( !( m_local->m_fFlags( ) & FL_ONGROUND ) )
				m_cmd->m_Buttons &= ~buttons::jump;
		}
	}
}



void Aimbot::think( )
{
	auto fire = [ & ] ( ) {
		if ( !( m_cmd->m_Buttons & buttons::attack ) )
			return;

		if ( !canFire( ) ) {
			m_cmd->m_Buttons &= ~buttons::attack;
			return;
		}

		if ( Vars::inst( ).misc.nospread ) {
			noSpread( );
		}

		if ( Vars::inst( ).misc.norecoil ) {
			static float amt = 1.f;
#if _CSGO
			amt = 2.f;
#elif _GMOD
			amt = 1.f;
#endif;	
			m_cmd->m_viewAngles -= m_local->m_vecPunchAngle( ) * amt;
		}

	};

	if ( Vars::inst( ).misc.active )
		misc( );

	if ( Vars::inst( ).aim.active || Vars::inst( ).misc.active && m_wep != nullptr ) {
		if ( Vars::inst( ).aim.active )
			aim( );

		fire( );
	}
}

void Aimbot::target( )
{
	static auto& fov = Vars::inst( ).aim.fov;
	auto max = fov ? fov.get( ) : 99999999.f;
	auto entMax = Client::inst( ).highestEntIndex( );

	for ( auto it = 1; it <= entMax; it++ ) {
		if ( targetable( it ) ) {
			auto delta = fov ? calcFov( m_cmd->m_viewAngles, m_local->eyePos( ), m_end ) : m_targetEnt->getAbsOrigin( ).get_distance( m_local->getAbsOrigin( ) );
			if ( delta < max ) {
				max = delta;
				m_target = it;
			}
		}
	}
}

void Aimbot::runOnce( )
{
	static bool runonce{ };
	if ( !runonce ) {
		Netvars::dumpIDs( STR( "classIDs.log" ) );

		runonce = true;
	}
}

void Aimbot::run( Cmd* cmd )
{
	if ( cmd == nullptr )
		return;

	if ( !Vars::inst( ).aim.active && !Vars::inst( ).misc.active )
		return;

	m_oldAngles = cmd->m_viewAngles;

	m_cmd = cmd;
	m_local = Client::localPly( );
	m_wep = m_local->getWep( );

	m_cmd->m_randomSeed = Client::inst( ).md5_pr( m_cmd->m_cmdNum ) & 0x7FFFFFFF; // gen random seed

	runOnce( );

	if ( Input::get( 'G' ) ) {
		std::cout << STR( "[Weapon] spread: " ) << m_wep->getBulletSpread( ) << std::endl;
	}

	think( );

	cmd->m_move = ( cmd->m_move.angle( ) + ( Vector3( 0.f, cmd->m_viewAngles[ 1 ], 0.f ) - Vector3( 0.f, m_oldAngles[ 1 ], 0.f ) ) ).fwd( ) * cmd->m_move.get_length( );
}
