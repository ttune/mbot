#include "triggerbot.h"
#include "../gui/m_input.h"

void Triggerbot::run( Cmd* cmd ) {
	if ( !cmd )
		return;

	m_cmd = cmd;
	
	m_local = Client::inst( ).localPly( );
	if ( !m_local )
		return;

	Vector3 shotEnd = m_cmd->m_viewAngles.fwd( );
	shotEnd = ( shotEnd * 8192 + m_local->eyePos( ) );

	SETrace tr = Trace::inst( ).trace( m_local->eyePos( ), shotEnd );
	
	data.m_fraction = tr.m_fraction;
	data.m_distance = tr.m_start.get_distance( tr.m_end );
	
	if ( !tr.m_entity )
		return;

	data.m_entity = tr.m_entity;
		
	auto* cl = tr.m_entity->getClass( );
	if ( !cl )
		return;

	data.m_entName = cl->m_pNetworkName;
	
#if _CSGO || _CSS
	if ( cl->m_nClassId != ClientClass::CCSPlayer )
		return;
#elif _HL2 || _GMOD
	if ( cl->m_nClassId != ClientClass::CAI_BaseNPC && 
		cl->m_nClassId != ClientClass::CNPC_Barnacle &&
		cl->m_nClassId != ClientClass::CNPC_Manhack && 
		cl->m_nClassId != ClientClass::CNPC_RollerMine && 
		cl->m_nClassId != ClientClass::CNPC_Strider && 
		cl->m_nClassId != ClientClass::CNPC_CombineGunship
#if _GMOD
		&& cl->m_nClassId != ClientClass::CGMOD_Player
#endif	
		)
		return;
#endif


	if ( Input::inst( ).mouse5( ) ) {
#if _CSGO || _CSS || _GMOD
		if ( data.m_entity->m_iTeamNum( ) == m_local->m_iTeamNum( ) )
			return;
#elif _HL2
		if ( data.m_entity->isFriendlyNpc( ) )
			return;
#endif
		m_cmd->m_Buttons |= buttons::attack;
	}
}
