#pragma once

#include "..\main\inc.h"

class Triggerbot {
public:
	struct triggerbotData {
		Entity* m_entity{ };
		float m_distance{ };
		float m_fraction{ };
		std::string m_entName{ };

		inline triggerbotData( ) { }
	};

private:
	Entity* m_local{ };
	Cmd* m_cmd{ };

	triggerbotData data{ };

public:
	inline Triggerbot( ) { }
	auto& getData( ) { return data; }

	void run( Cmd* );

	static Triggerbot& inst( ) {
		static Triggerbot* singleton = nullptr;
		if ( singleton == nullptr )
			singleton = new Triggerbot( );

		return *singleton;
	}
};