#pragma once

#include "../main/inc.h"
#include "../gui/m_input.h"
#include "vars.h"

class Aimbot {
private:
	Cmd* m_cmd{ };
	Entity* m_local{ }, *m_targetEnt{ };
	Weapon* m_wep{ };

	Vector3 m_oldAngles{ }, m_end{ }, m_spread{ };

	int m_target{ };

	float m_aimtime{ };

public:
	inline Aimbot( ) {};
	inline ~Aimbot( ) {};

	inline void drop( );
	inline void aim( );
	inline void iterativeSpreadFix( );
	inline void noSpread( );
	inline void misc( );
	inline void think( );

	inline bool targetable( int index );
	inline bool autofire( );

	inline Vector3& spread( ) { return m_spread; }

	void target( );

	void runOnce( );

	inline float calcFov( Vector3 va, Vector3 start, Vector3 end ) {
		Vector3 ang{ }, aim{ }, dir = ( end - start ).normalized( );
		
		auto vectorAngles = [ ] ( Vector3 fwd, Vector3& angles ) {
			if ( fwd[ 1 ] == 0.0f && fwd[ 0 ] == 0.0f ) {
				angles[ 0 ] = ( fwd[ 2 ] > 0.0f ) ? 270.0f : 90.0f;
				angles[ 1 ] = 0.0f;
			}
			else {
				float len2d = sqrt( square( fwd[ 0 ] ) + square( fwd[ 1 ] ) );

				angles[ 0 ] = deg( atan2f( -fwd[ 2 ], len2d ) );
				angles[ 1 ] = deg( atan2f( fwd[ 1 ], fwd[ 0 ] ) );

				if ( angles[ 0 ] < 0.0f ) angles[ 0 ] += 360.0f;
				if ( angles[ 1 ] < 0.0f ) angles[ 1 ] += 360.0f;
			}
			angles[ 2 ] = 0.0f;
		};

		auto makeVector = [ ] ( Vector3 in, Vector3& out ) {
			float p = rad( in[0] ), 
				y = rad( in[1] ),
				temp = cos( p );

			out[0] = -temp * -cos( y );
			out[1] = sin( y ) * temp;
			out[2] = -sin( p );
		};

		vectorAngles( dir, ang );
		makeVector( va, aim );
		makeVector( ang, ang );
		return Math::toDeg( acos( aim.dot_product( ang ) ) / aim.get_length_sqr( ) );

	}

	inline bool targetSwitched( ) {
		static int ent{ -1 };
		if ( ent == -1 )
			ent = m_target;

		if ( m_target != ent ) {
			ent = m_target;
			return true;
		}
		return false;
	}

	inline bool canFire( ) {
		if ( !m_wep->m_iClip1( ) ) {
			m_cmd->m_Buttons |= buttons::reload;
			return false;
		}
		else if ( !Input::get( 'R' ) ) {
			m_cmd->m_Buttons &= ~buttons::reload;
		}

		auto time = m_local->m_nTickBase( ) * Client::inst( ).globals( ).m_flIntervalPerTick;

		return m_local->m_flNextAttack( ) < time && m_wep->m_flNextPrimaryAttack( ) < time;
	}

	void run( Cmd* cmd );

	static Aimbot& inst( ) {
		static Aimbot* singleton{ };
		if ( singleton == nullptr )
			singleton = new Aimbot( );

		return *singleton;
	}
};