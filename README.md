# Multi game compat. Source Engine multihack #

This is a work in progress and relies on my API which you can find in my repos.

### Compatibility list ###
* CS:GO
* CS:S
* GMOD
* HL2
* HL2 DM

### Features ###
* Visuals
    * ESP
    * Boxes
    * Hitboxes
    * Health
    * Weapon
    * World (Pickups/etc)


* Aimbot
    * Target selection (Field of view/distance)
    * Aimpoint selection (damage)
    * Filters (NPCs, Teammates)
    * Smoothing (Aimtime scalar)


* Misc
    * Speedhack (CL_Move frame retn)
    * No Recoil
    * No Spread (p/y comp./ wav roll)
    * No Effects (Viewpunch)


* Screenshot
![hl2](https://bitbucket.org/repo/epKLrG/images/1723557354-OAGyGYh%5B1%5D.jpg)